#ifndef LASER_H_
#define LASER_H_
#include "Vec.h"
#include "Read.h"
#include <string>

struct Laser{
    double tau;
    double w0;
    std::string type;
    double vg;
    double focus_d1;
    double start_d1;
    double sigma0;
    double a0;
    double delay;
    double y0;
    double angle;
    double sina,cosa;
    
    Laser(const std::vector<std::string>& vecStringParams);
    void set_params_from_string(const std::string& line);
    bool is_work(double cTime, double t0, double z) const;
    bool is_work(double cTime) const;
    double get_Ey(double x, double y, long timestep) const;
    double get_Ez(double y, double times) const;

    double get_envelop(double z, double x, double cTime) const;
    double3 force(double2 coord, long timestep) const;
    double3 force_double(double2 coord, long timestep) const;
    double3 get_field_coll(double2 x, long timestep) const;
};

#endif 
