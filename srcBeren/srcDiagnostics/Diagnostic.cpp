#include "Diagnostic.h"

// Устанавливаем значения основных параметров в переменной Params в соответствии с содержимым сроки
void DiagData::set_params_from_string(const std::string& line){
    std::vector<std::string> strvec;

    strvec = split(line, ' ');

    if(strvec[0]=="sliceRadiationLineX") {
      for(ulong i = 1; i < strvec.size();i++ ){
        this->params.sliceRadiationLineX.push_back( stod(strvec[i] ) );
      }
    }
    if(strvec[0]=="sliceRadiationLineY") {
      for(ulong i = 1; i < strvec.size(); i++ ){
        this->params.sliceRadiationLineY.push_back( stod(strvec[i] ) );
      }
    }
    if(strvec[0]=="zondCoordsLineX") {
      for(ulong i = 1; i < strvec.size(); i++ ){
        this->params.zondCoordsLineX.push_back( stod(strvec[i] ) );
      }
    }

    if(strvec[0]=="zondCoordsLineY") {
      for(ulong i = 1; i < strvec.size() ; i++){
        this->params.zondCoordsLineY.push_back( stod(strvec[i] ) );
      }
    }

    if(strvec[0]=="zondCoords") {
      for(ulong i = 1; i < strvec.size(); i+=2 ){
        auto strx = strvec[i];
        auto stry = strvec[i+1];
        strx.erase(strx.find('('), 1);
        stry.erase(stry.find(')'), 1);
        this->params.zondCoords.push_back( double2(stod(strx ),
                                                    stod(stry ) ) );
      }
    }

}



void Writer::output(long timestep){
    diagData.calc_radiation_Pointing(_mesh,_world.region);
    diagData.calc_fields_avg_line(_mesh,_world,timestep);
    diagData.calc_radiation_Pointing_avg(_mesh,_world.region,timestep);

    diagData.calc_radiation_Pointing_half(_mesh,_world.region);
    diagData.calc_radiation_Pointing_avg_half(_mesh,_world.region,timestep);
  //return;

    if (timestep % TimeStepDelayDiag2D == 0){
        write_particles2D(timestep);
        write_fields2D(_mesh.fieldE, _mesh.fieldB, _mesh.fieldJ,timestep);
    }

    if (timestep % 2 == 0){

      diag_zond(timestep);
      diag_zond_lineX_bin(_mesh.fieldE, _mesh.fieldB, timestep);
      diag_zond_lineY_bin(_mesh.fieldE, _mesh.fieldB, timestep);
    }

    if (timestep % TimeStepDelayDiag1D == 0){
        write_radiation_line(timestep);
        write_radiation_avg_line(timestep);

        diagData.calc_energy(_mesh,_species);
        diagData.calc_energy_half(_mesh,_species);
        write_energies(timestep);
        write_energies_half(timestep);
        diagData.clear();                      
    }

    if (timestep % RecTimeStep == 0 && timestep != StartTimeStep){
        for (const auto& sp: _species){
            //sp.write_recovery(_world.MPIconf);
        }
        _mesh.write_recovery(_world.MPIconf);
    }
   // exit(0);
}


void make_folders(){
    mkdir(".//Fields", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Fields//Diag2D", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Fields//Diag1D", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Recovery", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Recovery//Fields", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Recovery//Particles", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Anime", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Particles", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Performance", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    std::cout << "Create folders for output...\n";
}

Writer::Writer(const World &world, const Mesh &mesh,std::vector<ParticlesArray> &species) : 
    _world(world),_mesh(mesh),_species(species),diagData(world.region) {
  if( !_world.MPIconf.is_master() ) return; 
  
  make_folders(); 
  for( const auto &sp : _species){
    mkdir((".//Particles//" + sp.name).c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir((".//Particles//" + sp.name+"//Diag2D").c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  }

  fDiagEnergies = fopen("Energies.dat", "w");
  fDiagEnergiesHalf = fopen("EnergiesHalf.dat", "w");
  
}


void DiagData::calc_fields_avg_line(const Mesh& mesh,const World& world, long timestep){
  long i,j,t;
  long sizeT = long(2 * PI / Dt);
  t = timestep % sizeT;
  auto size = mesh.fieldE.size();
  auto size_x = size(0);
  auto size_y = size(1);

    
    j = params.sliceRadiationLineY[0] / Dy;  
    for (i = 0; i < size_x - ADD_NODES;++i){
      fieldEAvgLineY[0](i,t) = mesh.get_fieldE_in_cell(i,j);
      fieldBAvgLineY[0](i,t) = mesh.get_fieldB_in_cell(i,j);
    }
    j = params.sliceRadiationLineY[1] / Dy;  
    for (i = 0; i < size_x - ADD_NODES;++i){
      fieldEAvgLineY[1](i,t) = mesh.get_fieldE_in_cell(i,j);
      fieldBAvgLineY[1](i,t) = mesh.get_fieldB_in_cell(i,j);
    } 
    
    i = (params.sliceRadiationLineX[0] - world.region.origin) / Dy; 
    if (! world.region.in_region(params.sliceRadiationLineX[0])) return;
    for (j = 0; j < size_y - ADD_NODES; ++j){
      fieldEAvgLineX[0](j,t) = mesh.get_fieldE_in_cell(i,j);
      fieldBAvgLineX[0](j,t) = mesh.get_fieldB_in_cell(i,j);
    }

    
  i = (params.sliceRadiationLineX[1] - world.region.origin) / Dx;
  if (!world.region.in_region(params.sliceRadiationLineX[1])) return;
    for (j = 0; j < size_y - ADD_NODES; ++j){
      fieldEAvgLineX[1](j,t) = mesh.get_fieldE_in_cell(i,j);
      fieldBAvgLineX[1](j,t) = mesh.get_fieldB_in_cell(i,j);
    }

}
void write_array2D(const Array2D<double>& data, long size1, long size2, const char* filename, const MPI_Topology& MPIconf){
    MPI_File fData2D;
    MPI_Status status;

    long sumSize1;
    float info;
    long  sizeData = size1*size2;
    
    if ( !MPIconf.is_master_depth() ) return;

    float* floatData = new float[size1*size2];

    MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fData2D);
    
    for( auto i = 0; i < size1; ++i ){
       for( auto j = 0; j < size2; ++j ){
          floatData[i*size2 + j] = float(data(i,j));
      }
    }


    sumSize1 = MPIconf.accum_sum_line(size1);

    if( MPIconf.is_last_line() ){
          info = float(sumSize1 + size1);
          MPI_File_write_at(fData2D, 0,&info, 1,MPI_FLOAT, &status);
          info = float(size2);
          MPI_File_write_at(fData2D, sizeof(float),&info, 1,MPI_FLOAT, &status);
    }
    long startWrite = sumSize1 * size2 * sizeof(float) + 2 * sizeof(float);
        
    MPI_File_write_at(fData2D, startWrite, floatData, sizeData, MPI_FLOAT, &status);
    MPI_File_close(&fData2D);
  
  delete[] floatData;

}
