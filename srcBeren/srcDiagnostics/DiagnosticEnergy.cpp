#include "Diagnostic.h"

void DiagData::calc_energy(const Mesh &mesh, const std::vector<ParticlesArray> &species){
  
  for ( auto &sp : species){
    energyParticlesKinetic[sp.name] = sp.get_kinetic_energy();
    energyParticlesInjectPrev[sp.name] = energyParticlesInject[sp.name];
    energyParticlesInject[sp.name] = sp.get_inject_energy();
  }

  energyFieldE = mesh.get_fieldE_energy();
  energyFieldB = mesh.get_fieldB_energy()- mesh.get_fieldB0_energy();


}

void DiagData::calc_energy_half(const Mesh &mesh, const std::vector<ParticlesArray> &species){
  
  for ( auto &sp : species){
    energyParticlesKineticHalf[sp.name] = sp.get_kinetic_energy_half();
  }

  energyFieldEHalf = mesh.get_fieldE_energy_half();
  energyFieldBHalf = mesh.get_fieldB_energy_half()- mesh.get_fieldB0_energy_half();

}


void Writer::write_energies(long timestep){
  std::stringstream ss;

  const MPI_Topology &MPIconf = _world.MPIconf;

  if(timestep == 0){
    ss << "Time ";
    for (auto it = diagData.energyParticlesKinetic.begin(); it != diagData.energyParticlesKinetic.end(); ++it){
      ss << "Area_" << it->first << " ";
    }
    for (auto it = diagData.energyParticlesInject.begin(); it != diagData.energyParticlesInject.end(); ++it){
      ss << "Injection_" << it->first << " ";
    }
    ss << "Area_E^2 " << "Area_B^2 " << "powerRadXmin "<< "powerRadXmax "<< "powerRadYmin " << "powerRadYmax " 
                      << "powerRadAvgXmin "<< "powerRadAvgXmax "<< "powerRadAvgYmin "<< "powerRadAvgYmax\n";
  }
  
  ss << timestep*Dt << " ";
    
  for (auto it = diagData.energyParticlesKinetic.begin(); it != diagData.energyParticlesKinetic.end(); ++it){
    double energyParticles = it->second;
    MPI_Allreduce ( MPI_IN_PLACE, &energyParticles, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD );
    ss << energyParticles << " ";
  }
  
  for (auto it = diagData.energyParticlesInject.begin(); it != diagData.energyParticlesInject.end(); ++it){
    double energyInject = it->second- diagData.energyParticlesInjectPrev[it->first];
    MPI_Allreduce ( MPI_IN_PLACE, &energyInject, 1, MPI_DOUBLE, MPI_SUM, MPIconf.comm_line() );
    ss << energyInject << " ";
  }
  std::vector<double> vecEnergy = { diagData.energyFieldE, diagData.energyFieldB,
              diagData.powerRadX[0], diagData.powerRadX[1],
              diagData.powerRadY[1], diagData.powerRadY[1],
              diagData.powerRadAvgX[0], diagData.powerRadAvgX[1],
              diagData.powerRadAvgY[0], diagData.powerRadAvgY[1] };
  
  MPI_Allreduce ( MPI_IN_PLACE, &vecEnergy[0], vecEnergy.size(), MPI_DOUBLE, MPI_SUM, MPIconf.comm_line() );

  for(uint i = 0; i< vecEnergy.size(); ++i){
  ss << vecEnergy[i] << " "; 
  }
  ss <<"\n";
  if( MPIconf.is_master() ){
      fprintf(fDiagEnergies, "%s",  ( ss.str() ).c_str() ); 
      std::cout << ss.str();
  }
    if( MPIconf.is_master() && timestep % TimeStepDelayDiag1D == 0){
      fflush(fDiagEnergies);
    }


}


void Writer::write_energies_half(long timestep){
  std::stringstream ss;

  const MPI_Topology &MPIconf = _world.MPIconf;

  if(timestep == 0){
    ss << "Time ";
    for (auto it = diagData.energyParticlesKinetic.begin(); it != diagData.energyParticlesKinetic.end(); ++it){
      ss << "Area_" << it->first << " ";
    }

    ss << "Area_E^2 " << "Area_B^2 " << "powerRadYmin " << "powerRadYmax " 
                      << "powerRadAvgYmin "<< "powerRadAvgYmax\n";
  }
  
  ss << timestep*Dt << " ";
    
  for (auto it = diagData.energyParticlesKineticHalf.begin(); it != diagData.energyParticlesKineticHalf.end(); ++it){
    double energyParticles = it->second;
    MPI_Allreduce ( MPI_IN_PLACE, &energyParticles, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD );
    ss << energyParticles << " ";
  }
  
  std::vector<double> vecEnergy = { diagData.energyFieldEHalf, diagData.energyFieldBHalf,
              diagData.powerRadHalfY[0], diagData.powerRadHalfY[1],
              diagData.powerRadAvgHalfY[0], diagData.powerRadAvgHalfY[1] };
  
  MPI_Allreduce ( MPI_IN_PLACE, &vecEnergy[0], vecEnergy.size(), MPI_DOUBLE, MPI_SUM, MPIconf.comm_line() );

  for(uint i = 0; i< vecEnergy.size(); ++i){
  ss << vecEnergy[i] << " "; 
  }
  ss <<"\n";
  if( MPIconf.is_master() ){
      fprintf(fDiagEnergiesHalf, "%s",  ( ss.str() ).c_str() ); 
      std::cout << ss.str();
  }
    if( MPIconf.is_master() && timestep % TimeStepDelayDiag1D == 0){
      fflush(fDiagEnergiesHalf);
    }


}


void DiagData::calc_radiation_Pointing(const Mesh &mesh,const Region &region){
  long i,j;
  double3 fieldEAvg;
  double3 fieldBAvg;
  
  powerRadX = {0,0};
  powerRadY = {0,0};
  
  if(region.boundType_d1[0] == OPEN){
      i = (params.sliceRadiationLineX[0] - region.origin) / Dx;
      for( j = 0; j < region.numCells_d2; j++){
          fieldEAvg = mesh.get_fieldE_in_cell(i,j);
          fieldBAvg = mesh.get_fieldB_in_cell(i,j);
          powerRadLineX[0](j) += Dt*Dy*fabs(fieldBAvg.z()*fieldEAvg.y()-fieldEAvg.z()*fieldBAvg.y());
          powerRadX[0] += powerRadLineX[0](j);
        }
    }
    if(region.boundType_d1[1] == OPEN){
        i = (params.sliceRadiationLineX[1] - region.origin) / Dx;
        for( j = 0; j < region.numCells_d2 ; j++){
          fieldEAvg = mesh.get_fieldE_in_cell(i,j);
          fieldBAvg = mesh.get_fieldB_in_cell(i,j);
          powerRadLineX[1](j) += Dt*Dy*fabs(fieldBAvg.z()*fieldEAvg.y()-fieldEAvg.z()*fieldBAvg.y());
          powerRadX[1] += powerRadLineX[1](j);
        }
    }
    
    j = params.sliceRadiationLineY[0] / Dy;
    for (i = 0; i < region.numCells_d1 ; i++){
        fieldEAvg = mesh.get_fieldE_in_cell(i,j);
        fieldBAvg = mesh.get_fieldB_in_cell(i,j);
        powerRadLineY[0](i) += Dt*Dx * fabs(fieldEAvg.x()*fieldBAvg.z()-fieldEAvg.z()*fieldBAvg.x());
        powerRadY[0] += powerRadLineY[0](i);
    }
    j = params.sliceRadiationLineY[1] / Dy;
    for (i = 0; i < region.numCells_d1 ; i++){
        fieldEAvg = mesh.get_fieldE_in_cell(i,j);
        fieldBAvg = mesh.get_fieldB_in_cell(i,j);
        powerRadLineY[1](i) += Dt*Dx * fabs(fieldEAvg.x()*fieldBAvg.z()-fieldEAvg.z()*fieldBAvg.x());
        powerRadY[1] += powerRadLineY[1](i);
    }    
}

void DiagData::calc_radiation_Pointing_half(const Mesh &mesh,const Region &region){
  long i;
  double3 fieldEAvg;
  double3 fieldBAvg;
  
  powerRadHalfY = {0,0};  
    
    for (i = 0; i < region.numCells_d1 ; i++){
      double coordX = region.get_coord_from_index(i);
      double2 coordGlob =  region.get_coord_glob(double2(coordX,0.));
      if( coordGlob.x() > HalfPointCoordX ) continue;
        powerRadHalfY[0] += powerRadLineY[0](i);
        powerRadHalfY[1] += powerRadLineY[1](i);
    }    
}


void DiagData::calc_radiation_Pointing_avg(const Mesh &mesh,const Region &region,long timestep){
  long i,j,t;
  long sizeT = long(2 * PI / Dt);
  long t1 = timestep % sizeT;
  double3 fieldE, fieldB;
  double pRad;
  powerRadAvgX = {0.,0.};
  powerRadAvgY = {0.,0.};

  if(region.boundType_d1[0] == OPEN){
      i = (params.sliceRadiationLineX[0] - region.origin) / Dx;
      for( j = 0; j < region.numCells_d2; j++){
          fieldE = fieldB = 0.;
          for( t =  0; t < fieldEAvgLineX[0].size().y(); t++){
            fieldE += fieldEAvgLineX[0](j,t);
            fieldB += fieldBAvgLineX[0](j,t);
          }

          fieldE = mesh.get_fieldE_in_cell(i,j)
                              - Dt/(2.*PI)*fieldE;
          fieldB = mesh.get_fieldB_in_cell(i,j)
                              - Dt/(2.*PI)*fieldB;
          powerRadAvgLineX[0](j,t1) =  Dt*Dy*fabs(fieldB.z()*fieldE.y()-fieldE.z()*fieldB.y());
        }
    }
    if(region.boundType_d1[1] == OPEN){
      i = (params.sliceRadiationLineX[1]-region.origin) / Dx;
      for( j = 0; j < region.numCells_d2 ; j++){
          fieldE = fieldB = 0.;
          for( t =  0; t < fieldEAvgLineX[1].size().y(); t++){
            fieldE += fieldEAvgLineX[1](j,t);
            fieldB += fieldBAvgLineX[1](j,t);
          }
          fieldE = mesh.get_fieldE_in_cell(i,j) 
                            - Dt/(2.*PI)*fieldE;
          fieldB = mesh.get_fieldB_in_cell(i,j)
                             - Dt/(2.*PI)*fieldB;
          powerRadAvgLineX[1](j,t1) = Dt*Dy*fabs(fieldB.z()*fieldE.y()-fieldE.z()*fieldB.y());

        }
    }
    j = params.sliceRadiationLineY[0] / Dy;
    for ( i = 0; i < region.numCells_d1; i++){
         fieldE =  fieldB = 0.;
        for( t =  0; t < fieldEAvgLineY[0].size().y(); t++){
            fieldE += fieldEAvgLineY[0](i,t);
            fieldB += fieldBAvgLineY[0](i,t);
        }
        fieldE = mesh.get_fieldE_in_cell(i,j)
                          - Dt/(2.*PI)*fieldE;
        fieldB = mesh.get_fieldB_in_cell(i,j)
                          - Dt/(2.*PI)*fieldB;
        powerRadAvgLineY[0](i,t1) =  Dt*Dx * fabs(fieldE.x()*fieldB.z()-fieldE.z()*fieldB.x());
    }
    j = params.sliceRadiationLineY[1] / Dy;
    for ( i = 0; i < region.numCells_d1; i++){
         fieldE =  fieldB = 0.;
        for( t =  0; t < fieldEAvgLineY[1].size().y(); t++){
            fieldE += fieldEAvgLineY[1](i,t);
            fieldB += fieldBAvgLineY[1](i,t);
        }
        fieldE = mesh.get_fieldE_in_cell(i,j)
                          - Dt/(2.*PI)*fieldE;
        fieldB = mesh.get_fieldB_in_cell(i,j)
                          - Dt/(2.*PI)*fieldB;
        powerRadAvgLineY[1](i,t1) =  Dt*Dx * fabs(fieldE.x()*fieldB.z()-fieldE.z()*fieldB.x());
    }

    for( i = region.dampCells_d1[0]; i < region.numCells_d1 - region.dampCells_d1[1];i++){
      pRad = powerRadAvgLineY[0].sum_d2(i)/(2*PI);
      powerRadAvgY[0] += pRad;
      pRad = powerRadAvgLineY[1].sum_d2(i)/(2*PI);
      powerRadAvgY[1] += pRad;
    }
    
    for(i = region.dampCells_d2; i < region.numCells_d2 - region.dampCells_d2; i++){
      pRad = powerRadAvgLineX[0].sum_d2(i)/(2*PI);
      powerRadAvgX[0] += pRad;        
      pRad = powerRadAvgLineX[1].sum_d2(i)/(2*PI);
      powerRadAvgX[1] += pRad;     
    }
}

void DiagData::calc_radiation_Pointing_avg_half(const Mesh &mesh,const Region &region,long timestep){
  long i;
  double3 fieldE, fieldB;
  double pRad;
  powerRadAvgHalfY = {0.,0.};

    for( i = 0; i < region.numCells_d1 ;i++){
      double coordX = region.get_coord_from_index(i);
      double2 coordGlob =  region.get_coord_glob(double2(coordX,0.));
      if( coordGlob.x() > HalfPointCoordX ) continue;
      
      pRad = powerRadAvgLineY[0].sum_d2(i)/(2*PI);
      powerRadAvgHalfY[0] += pRad;
      pRad = powerRadAvgLineY[1].sum_d2(i)/(2*PI);
      powerRadAvgHalfY[1] += pRad;
    }
}


void Writer::write_line_dataY(const Array1D<double>& array, long sizeData,const std::string& fname, MPI_File& mfile ,const MPI_Topology &MPIconf , long currentStep){
   
    if (!MPIconf.is_master_depth()) return;

    MPI_Status status;
   // char filename[80];
    float* floatData = new float[sizeData];

    //long startWrite, sizeWrite;
    static float info;
    long i; 
    static long accum_sizeData;
    if( currentStep == 0){
        //sprintf(filename, fname);
        MPI_File_open(MPIconf.comm_line(), fname.c_str(), MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &mfile);
        //info = float(sizeData*MPIconf.size_line());
        accum_sizeData = MPIconf.accum_sum_line(sizeData);
        info = float(accum_sizeData+sizeData);// float(sizeData* MPIconf.size_line() );
        if(MPIconf.is_last_line()){   
            MPI_File_write_at(mfile, 0, &info, 1, MPI_FLOAT, &status);
        }
        
      MPI_Barrier(MPIconf.comm_line());
    }

    for( i = 0; i < sizeData; i++){
      floatData[i] = float(array(i) );
    }

    long sizeWrite = info*sizeof(float);
    long startWrite = accum_sizeData*sizeof(float) + sizeof(float);


    //startWrite = MPIconf.rank_line()*sizeData*sizeof(float) + sizeof(float);
    //sizeWrite = MPIconf.size_line()*sizeData*sizeof(float);
    startWrite += sizeWrite*currentStep;

    MPI_File_write_at(mfile, startWrite, floatData, sizeData, MPI_FLOAT, &status);
        delete[] floatData;

}

void Writer::write_line_dataX(const Array1D<double>& array, long sizeData, const std::string& fname, MPI_File& mfile ,const MPI_Topology &MPIconf , long currentStep){
   
    MPI_Status status;
    //char filename[80];
    float* floatData = new float[sizeData];

    long startWrite;
    float info;
    long i; 

    if( currentStep == 0){
        //sprintf(filename, fname);
        MPI_File_open(MPIconf.comm_depth(), fname.c_str(), MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &mfile);
        info = float(sizeData);
           if (MPIconf.is_master_depth())
            MPI_File_write_at(mfile, 0, &info, 1, MPI_FLOAT, &status);
    }

    startWrite = sizeData*sizeof(float)*currentStep;

        for(i = 0; i < sizeData; i++){
            floatData[i] = float(array(i) );
        }
        if (MPIconf.is_master_depth())
        MPI_File_write_at(mfile, startWrite + sizeof(float), floatData, sizeData, MPI_FLOAT, &status);
        delete[] floatData;
}



void Writer::write_radiation_line(long timestep){
    const Region &region = _world.region;
    const MPI_Topology &MPIconf = _world.MPIconf;

    static MPI_File fRadX_min,fRadX_max,fRadY_min,fRadY_max;
    static long currentStep = 0; 

    write_line_dataY(diagData.powerRadLineY[0], region.numCells_d1,".//Fields//Diag1D//PRadY_0.bin", fRadY_min,MPIconf ,currentStep);
    write_line_dataY(diagData.powerRadLineY[1], region.numCells_d1,".//Fields//Diag1D//PRadY_1.bin", fRadY_max,MPIconf ,currentStep);

    if(region.boundType_d1[0]==OPEN){
          write_line_dataX(diagData.powerRadLineX[0], region.numCells_d2,".//Fields//Diag1D//PRadX_0.bin", fRadX_min,MPIconf ,currentStep);
    }
    if(region.boundType_d1[1]==OPEN){
          write_line_dataX(diagData.powerRadLineX[1], region.numCells_d2,".//Fields//Diag1D//PRadX_1.bin", fRadX_max,MPIconf ,currentStep);
    }
    currentStep++;
}

void Writer::write_radiation_avg_line(long timestep){
    const Region &region = _world.region;
    const MPI_Topology &MPIconf = _world.MPIconf;

    static MPI_File fRadX_min,fRadX_max,fRadY_min,fRadY_max;

    Array1D<double> doubleDataY(region.numCells_d1);
    Array1D<double> doubleDataX(region.numCells_d2);
    static long currentStep = 0; 
    
    for(auto i = 0; i < region.numCells_d1; i++){
        doubleDataY(i) = float( diagData.powerRadAvgLineY[0].sum_d2(i)/(2*PI) );
    }
    
    write_line_dataY(doubleDataY, region.numCells_d1,".//Fields//Diag1D//PRadAvgY_0.bin", fRadX_min,MPIconf ,currentStep);
    for(auto i = 0; i < region.numCells_d1; i++){
        doubleDataY(i) = float( diagData.powerRadAvgLineY[1].sum_d2(i)/(2*PI) );
    }
    
    write_line_dataY(doubleDataY, region.numCells_d1,".//Fields//Diag1D//PRadAvgY_1.bin", fRadX_max,MPIconf ,currentStep);

    if(region.boundType_d1[0]==OPEN){
    for(auto i = 0; i < region.numCells_d2; i++){
        doubleDataX(i) = float( diagData.powerRadAvgLineX[0].sum_d2(i)/(2*PI) );
    }
    
          write_line_dataX(doubleDataX, region.numCells_d2,".//Fields//Diag1D//PRadAvgY_0.bin", fRadY_min,MPIconf ,currentStep);
    }
    if(region.boundType_d1[1]==OPEN){
        for(auto i = 0; i < region.numCells_d2; i++){
        doubleDataX(i) = float( diagData.powerRadAvgLineX[1].sum_d2(i)/(2*PI) );
    }
    
          write_line_dataX(doubleDataX, region.numCells_d2,".//Fields//Diag1D//PRadAvgY_1.bin", fRadY_max,MPIconf ,currentStep);
    }
    currentStep++;

}
/*
void Writer::write_radiation_line(long timestep){
    const Region &region = _world.region;
    const MPI_Topology &MPIconf = _world.MPIconf;

    if (!MPIconf.is_master_depth()) return;

    static MPI_File fRadX_min,fRadX_max,fRadY_min,fRadY_max;
    MPI_Status status;
    char filename[80];
    long sizeDataX = (region.numCells_d1);// - region.dampCells_d1[0] - region.dampCells_d1[1]);
    long sizeDataY = (region.numCells_d2);
    static float* floatDataX = new float[sizeDataX];
    static float* floatDataY = new float[sizeDataY];
    static long currentStep = 0; 
    long startWrite, sizeWrite;
    float info;
    long i,indx; 

    if( timestep == StartTimeStep){
        sprintf(filename, ".//Fields//Diag1D//PRadY_min.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadY_min);
        info = float(sizeDataX*MPIconf.size_line());
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadY_min, 0, &info, 1, MPI_FLOAT, &status);
        }
        sprintf(filename, ".//Fields//Diag1D//PRadY_max.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadY_max);
        info = float(sizeDataX*MPIconf.size_line());
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadY_max, 0, &info, 1, MPI_FLOAT, &status);
        }
        sprintf(filename, ".//Fields//Diag1D//PRadX_min.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadX_min);
        info = float(sizeDataY);
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadX_min, 0, &info, 1, MPI_FLOAT, &status);
        }

        sprintf(filename, ".//Fields//Diag1D//PRadX_max.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadX_max);
        info = float(sizeDataY);
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadX_max, 0, &info, 1, MPI_FLOAT, &status);
        }
      MPI_Barrier(MPIconf.comm_line());
    }

    for( i = 0; i < sizeDataX; i++){
      indx = region.dampCells_d1[0];
      floatDataX[i] = float(diagData.powerRadLine.y_min(i+indx) );
     //             std::cout << i<< " " << i+indx << "\n";

    }
    startWrite = MPIconf.rank_line()*sizeDataX*sizeof(float) + sizeof(float);
    sizeWrite = MPIconf.size_line()*sizeDataX*sizeof(float);
    startWrite += sizeWrite*currentStep;

    MPI_File_write_at(fRadY_min, startWrite, floatDataX, sizeDataX, MPI_FLOAT, &status);
    
    for( i = 0; i < sizeDataX; i++){
     // indx = region.dampCells_d1[0];
      //floatDataX[i] = float(diagData.powerRadLine.y_max(i+indx) );
      floatDataX[i] = float(diagData.powerRadLine.y_max(i+indx) );
    }
    MPI_File_write_at(fRadY_max, startWrite, floatDataX, sizeDataX, MPI_FLOAT, &status);


    startWrite = sizeDataY*sizeof(float)*currentStep;
    
    if(region.boundType_d1[0]==OPEN){
        for(i = 0; i < sizeDataY; i++){
            //indx = region.dampCells_d2;
            //floatDataY[i] = float(diagData.powerRadLine.x_min(i+indx) );
            floatDataY[i] = float(diagData.powerRadLine.x_min(i+indx) );
        }
        MPI_File_write_at(fRadX_min, startWrite + sizeof(float), floatDataY, sizeDataY, MPI_FLOAT, &status);
    }

    if(region.boundType_d1[1]==OPEN){
        for(i = 0; i < sizeDataY; i++){
            //indx = region.dampCells_d2;            
            //floatDataY[i] = float(diagData.powerRadLine.x_max(i+indx) );
            floatDataY[i] = float(diagData.powerRadLine.x_max(i+indx) );
        }
        MPI_File_write_at(fRadX_max, startWrite + sizeof(float), floatDataY, sizeDataY, MPI_FLOAT, &status);
    }

    currentStep++;
}

void Writer::write_radiation_avg_line(long timestep){
    const Region &region = _world.region;
    const MPI_Topology &MPIconf = _world.MPIconf;

    if (!MPIconf.is_master_depth()) return;

    static MPI_File fRadX_min,fRadX_max,fRadY_min,fRadY_max;
    MPI_Status status;

    char filename[50];
    long sizeDataX = (region.numCells_d1 - region.dampCells_d1[0] - region.dampCells_d1[1]);
    long sizeDataY = (region.numCells_d2 - 2*region.dampCells_d2);
    static float* floatDataX = new float[sizeDataX];
    static float* floatDataY = new float[sizeDataY];
    static long currentStep = 0; 
    int startWrite, sizeWrite;
    float info;
    long i,indx; 

    if( timestep == StartTimeStep){
        sprintf(filename, ".//Fields//Diag1D//PRadAvgY_min.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadY_min);
        info = float(sizeDataX*MPIconf.size_line());
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadY_min, 0, &info, 1, MPI_FLOAT, &status);
        }
        sprintf(filename, ".//Fields//Diag1D//PRadAvgY_max.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadY_max);
        info = float(sizeDataX*MPIconf.size_line());
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadY_max, 0, &info, 1, MPI_FLOAT, &status);
        }

        sprintf(filename, ".//Fields//Diag1D//PRadAvgX_min.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadX_min);
        info = float(sizeDataY);
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadX_min, 0, &info, 1, MPI_FLOAT, &status);
        }

        sprintf(filename, ".//Fields//Diag1D//PRadAvgX_max.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadX_max);
        info = float(sizeDataY);
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadX_max, 0, &info, 1, MPI_FLOAT, &status);
        }
    }

    for( i = 0; i < region.numCells_d1 - region.dampCells_d1[1];i++){
      indx = region.dampCells_d1[0];
      floatDataX[i] = float( diagData.powerRadAvgLine.y_min.sum_d2(i+indx)/(2*PI) );
    }
    startWrite = MPIconf.rank_line()*sizeDataX*sizeof(float) + sizeof(float);
    sizeWrite = MPIconf.size_line()*sizeDataX*sizeof(float);
    startWrite += sizeWrite*currentStep;

    MPI_File_write_at(fRadY_min, startWrite, floatDataX, sizeDataX, MPI_FLOAT, &status);

    for( i = 0; i < region.numCells_d1 - region.dampCells_d1[1];i++){
      indx = region.dampCells_d1[0];      
      floatDataX[i] = float( diagData.powerRadAvgLine.y_min.sum_d2(i+indx)/(2*PI) );
    }
    MPI_File_write_at(fRadY_max, startWrite, floatDataX, sizeDataX, MPI_FLOAT, &status);


    startWrite = sizeDataY*sizeof(float)*currentStep;
    
    if(region.boundType_d1[0]==OPEN){
        for(i = 0; i < region.numCells_d2 - region.dampCells_d2; i++){
            indx = region.dampCells_d2;            
            floatDataY[i] = float( diagData.powerRadAvgLine.x_min.sum_d2(i+indx)/(2*PI) );
        }
        MPI_File_write_at(fRadX_min, startWrite + sizeof(float), floatDataY, sizeDataY, MPI_FLOAT, &status);
    }

    if(region.boundType_d1[1]==OPEN){
        for(i = 0; i < region.numCells_d2 - region.dampCells_d2; i++){
            indx = region.dampCells_d2;            
            floatDataY[i] = float( diagData.powerRadAvgLine.x_max.sum_d2(i+indx)/(2*PI) );
        }
        MPI_File_write_at(fRadX_max, startWrite + sizeof(float), floatDataY, sizeDataY, MPI_FLOAT, &status);
    }

    currentStep++;
}
*/