#include "Diagnostic.h"
#include <vector>

void Writer::diag_zond(long timestep){
  if (!_world.MPIconf.is_master_depth()) return;
  
  char filename[50];
  static FILE *fZond; 
  long i,j;
  double xx,yy;  
  double3 E,B;


  if (timestep == StartTimeStep){
    sprintf(filename, "./Fields/Zond%04d.dat",_world.MPIconf.rank_line() );
    fZond = fopen(filename, "w");
    fprintf(fZond, "%s ", "## timestep ");
    for (ulong n = 0; n < diagData.params.zondCoords.size(); ++n){
      
      if( ! _world.region.in_region(diagData.params.zondCoords[n].x() ) ) continue;
      xx = diagData.params.zondCoords[n].x();
      yy = diagData.params.zondCoords[n].y();
      fprintf(fZond, "%s%g%s%g%s %s%g%s%g%s %s%g%s%g%s %s%g%s%g%s %s%g%s%g%s %s%g%s%g%s ", 
          "Ex(",xx,",",yy,")","Ey(",xx,",",yy,")","Ez(",xx,",",yy,")","Bx(",xx,",",yy,")","By(",xx,",",yy,")","Bz(",xx,",",yy,")"  );
    }
    fprintf(fZond, "\n");
  }
  
    fprintf(fZond, "%g ",Dt*timestep);
    
    for (ulong n = 0; n < diagData.params.zondCoords.size(); ++n){
      if( ! _world.region.in_region( diagData.params.zondCoords[n].x() ) ) continue;
      xx = diagData.params.zondCoords[n].x() -  _world.region.origin;
      yy = diagData.params.zondCoords[n].y();
      i = long(xx / Dx);
      j = long(yy / Dy);
      E = _mesh.get_fieldE_in_cell(i,j);
      B = _mesh.get_fieldB_in_cell(i,j);
      fprintf(fZond, "%g %g %g %g %g %g ",  E.x(), E.y(), E.z(), B.x(), B.y(), B.z() );
    }
    
    fprintf(fZond, "\n");
    if(  timestep % TimeStepDelayDiag1D == 0){
      fflush(fZond);
    }

}

static void write_zond_lineY_bin(const Array2D<double3>& fieldE, long dim, long indy,
      const std::string& fname,const MPI_Topology& MPIconf, const Region& domain, MPI_File& fZondLine, long currentStep){
    
    MPI_Status status;
    long numCells = (domain.numCells_d1);// - domain.dampCells_d1[0] - domain.dampCells_d1[1]) ;
    //auto sizeData = (domain.numCells_d1 - domain.dampCells_d1[0] - domain.dampCells_d1[1]);
    auto sizeData = numCells;
    long indx;
    
    static float* floatData = new float[sizeData];
    static long accum_sizeData;
    static float info;
    static long sizeWrite;

    if( currentStep == 0){
        //sprintf(filename, "./Fields/ZondLineY_node%04d.bin",(int)indy);
        MPI_File_open(MPIconf.comm_line(), fname.c_str(), MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fZondLine);
        accum_sizeData = MPIconf.accum_sum_line(sizeData);
        info = float(accum_sizeData+sizeData );
    
        if(MPIconf.is_last_line()){   
            MPI_File_write_at(fZondLine, 0, &info, 1, MPI_FLOAT, &status);
        }
        sizeWrite = (accum_sizeData+sizeData)*sizeof(float);
        MPI_Bcast(&sizeWrite, 1, MPI_LONG, MPIconf.last_line(), MPIconf.comm_line());

    }

    for( auto i = 0; i < numCells; ++i){
        indx =  i;// + domain.dampCells_d1[0];
        if (dim==0)
            floatData[i] = float(fieldE(indx,indy).x() );
        if (dim==1)
            floatData[i] = float(fieldE(indx,indy).y() );
        if (dim==2)
            floatData[i] = float(fieldE(indx,indy).z() );                    
    }
  
    //long startWrite = MPIconf.rank_line()*sizeData*sizeof(float) + sizeof(float);
  
    //long sizeWrite = MPIconf.size_line()*sizeData*sizeof(float);
    long startWrite = accum_sizeData*sizeof(float) + sizeof(float);
  
    startWrite += sizeWrite*currentStep;
    
    MPI_File_write_at(fZondLine, startWrite, floatData, sizeData, MPI_FLOAT, &status);
      
    //delete[] floatData;
}

void Writer::diag_zond_lineY_bin(const Array2D<double3>& fieldE, const Array2D<double3>& fieldB, long timestep){
  auto numZonds = diagData.params.zondCoordsLineY.size();
  if (!_world.MPIconf.is_master_depth() ||  numZonds <= 0) return;
  
  long indy;
  static std::vector<MPI_File> fZondLineE0,fZondLineE1,fZondLineE2;
  static std::vector<MPI_File> fZondLineB0,fZondLineB1,fZondLineB2;
  static std::vector<long> currentStep; 
  if( timestep == StartTimeStep){
    for (ulong n = 0; n < numZonds; ++n ){
        MPI_File file;
        fZondLineE0.push_back(file);
        fZondLineE1.push_back(file);
        fZondLineE2.push_back(file);
        fZondLineB0.push_back(file);
        fZondLineB1.push_back(file);
        fZondLineB2.push_back(file);
        currentStep.push_back(0);
    }
  }
   
  for (ulong n = 0; n < numZonds; ++n ){
        auto coord = diagData.params.zondCoordsLineY[n];

    indy = long( coord / Dy);
    write_zond_lineY_bin(_mesh.fieldE, 0, indy,"./Fields/Ex_ZondLineY_"+std::to_string(indy)+".bin", _world.MPIconf, _world.region, fZondLineE0[n], currentStep[n]);
    write_zond_lineY_bin(_mesh.fieldE, 1, indy,"./Fields/Ey_ZondLineY_"+std::to_string(indy)+".bin", _world.MPIconf, _world.region, fZondLineE1[n], currentStep[n]);
    write_zond_lineY_bin(_mesh.fieldB, 2, indy,"./Fields/Bz_ZondLineY_"+std::to_string(indy)+".bin", _world.MPIconf, _world.region, fZondLineB2[n], currentStep[n]);
    currentStep[n]++;
  }
  
}

static void write_zond_lineX_bin(const Array2D<double3>& fieldE, long dim, long indx,
      const std::string& fname,const MPI_Topology& MPIconf, const Region& domain, MPI_File& fZondLine, long currentStep){
    
    MPI_Status status;
    long numCells = domain.numCells_d2 ;//- 2*domain.dampCells_d2) ;
    //auto sizeData = (domain.numCells_d2) ;
    auto sizeData = numCells;
    long indy;
    
    static float* floatData = new float[sizeData];
   

    if( currentStep == 0){
        MPI_File_open(MPIconf.comm_depth(), fname.c_str(), MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fZondLine);
        float info = float(sizeData );
        if( MPIconf.is_master_depth() )
            MPI_File_write_at(fZondLine, 0, &info, 1, MPI_FLOAT, &status);
    }

    for( auto i = 0; i < sizeData; ++i){
        indy = i;//+domain.dampCells_d2;
        if (dim==0)
            floatData[i] = float(fieldE(indx,indy).x() );
        if (dim==1)
            floatData[i] = float(fieldE(indx,indy).y() );
        if (dim==2)
            floatData[i] = float(fieldE(indx,indy).z() );                    
    }
  
    long startWrite = sizeData*sizeof(float)*currentStep+ sizeof(float);  
        if( MPIconf.is_master_depth() )
            MPI_File_write_at(fZondLine, startWrite, floatData, sizeData, MPI_FLOAT, &status);
      
    //delete[] floatData;
}

void Writer::diag_zond_lineX_bin(const Array2D<double3>& fieldE, const Array2D<double3>& fieldB, long timestep){
  auto numZonds = diagData.params.zondCoordsLineX.size();
  if (  numZonds <= 0) return;
  
  long indx,indxGlob;
  static std::vector<MPI_File> fZondLineE0,fZondLineE1,fZondLineE2;
  static std::vector<MPI_File> fZondLineB0,fZondLineB1,fZondLineB2;
  static std::vector<long> currentStep; 
  if( timestep == StartTimeStep){
    for (ulong n = 0; n < numZonds; ++n ){
        MPI_File file;
        fZondLineE0.push_back(file);
        fZondLineE1.push_back(file);
        fZondLineE2.push_back(file);
        fZondLineB0.push_back(file);
        fZondLineB1.push_back(file);
        fZondLineB2.push_back(file);
        currentStep.push_back(0);
    }
  }
   
  for (ulong n = 0; n < numZonds; ++n ){
    auto coord = diagData.params.zondCoordsLineX[n];

    if( ! _world.region.in_region(coord) ) continue;

    indx = long( (coord - _world.region.origin) / Dx);
    indxGlob = indx+long( _world.region.origin / Dx);
    write_zond_lineX_bin(_mesh.fieldE, 0, indx,"./Fields/Ex_Zond_LineX_"+std::to_string(indxGlob)+".bin", _world.MPIconf, _world.region, fZondLineE0[n], currentStep[n]);
    write_zond_lineX_bin(_mesh.fieldE, 1, indx,"./Fields/Ey_Zond_LineX_"+std::to_string(indxGlob)+".bin", _world.MPIconf, _world.region, fZondLineE1[n], currentStep[n]);
    write_zond_lineX_bin(_mesh.fieldB, 2, indx,"./Fields/Bz_Zond_LineX_"+std::to_string(indxGlob)+".bin", _world.MPIconf, _world.region, fZondLineB2[n], currentStep[n]);
    currentStep[n]++;
  }
  
}
/*
static void write_zond_lineX_bin(const Array2D<double3>& fieldE, const Array2D<double3>& fieldB, long indx,
               const MPI_Topology& MPIconf, const Region& domain, MPI_File& fZondLine, long currentStep,long timestep){
    
    MPI_Status status;
    char filename[100];
    long numCells = (domain.numCells_d2 - 2*domain.dampCells_d2) ;
    auto sizeData = 6*numCells;
    long indy;
    std::cout<<sizeData<<"\n";
    static float* floatData = new float[sizeData];

    //static long currentStep = 0; 

    if( timestep == StartTimeStep){
        sprintf(filename, "./Fields/ZondLineX_node%04d.bin",(int)indx + int(domain.origin/Dx) );

        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fZondLine);
        float info = float(numCells);
    
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fZondLine, 0, &info, 1, MPI_FLOAT, &status);
        }
    }

    for( auto j = 0; j < numCells; ++j){
      indy = j+domain.dampCells_d2;
        floatData[6 * j    ] = float(fieldE(indx,indy).x() );
        floatData[6 * j + 1] = float(fieldE(indx,indy).y() );
        floatData[6 * j + 2] = float(fieldE(indx,indy).z() );
        floatData[6 * j + 3] = float(fieldB(indx,indy).x() );
        floatData[6 * j + 4] = float(fieldB(indx,indy).y() );
        floatData[6 * j + 5] = float(fieldB(indx,indy).z() );
    }
  
    long startWrite = MPIconf.rank_line()*sizeData*sizeof(float) + sizeof(float);
  
    long sizeWrite = MPIconf.size_line()*sizeData*sizeof(float);
    
    startWrite += sizeWrite*currentStep;

    MPI_File_write_at(fZondLine, startWrite ,floatData, sizeData, MPI_FLOAT, &status);
   // delete[] floatData;
}

void Writer::diag_zond_lineX_bin(const Array2D<double3>& fieldE, const Array2D<double3>& fieldB, long timestep){
  ulong numZonds = diagData.params.zondCoordsLineY.size();
  if (! _world.MPIconf.is_master_depth() ||  numZonds <= 0) return;
  long indx;
  static std::vector<MPI_File> fZondLine;
  static std::vector<long> currentStep; 
  if( timestep == StartTimeStep){
    for (ulong n = 0; n < numZonds; ++n ){
        MPI_File file;
        fZondLine.push_back(file);
        currentStep.push_back(0);
    }
  }
   
  for (ulong n = 0; n < numZonds; ++n ){
        auto coord = diagData.params.zondCoordsLineX[n];
    if( ! _world.region.in_region(coord) ) continue;

    indx = long( coord / Dx);    
    write_zond_lineX_bin(_mesh.fieldE, _mesh.fieldB, indx, _world.MPIconf, _world.region, fZondLine[n], currentStep[n],timestep);
    currentStep[n]++;
  }
  
}

void Writer::write_fields2D(const Array2D<double3>& fieldE, const Array2D<double3>& fieldB, const long& timestep){
    if (!_world.MPIconf.is_master_depth()) return;
    
    MPI_File fField2D;
    MPI_Status status;
    
    char filename[50];
    float info;
    long size_x = fieldE.size()(0) - ADD_NODES;
    long size_y = fieldE.size()(1);
    int sizeData = size_x *size_y;
    long sizeWriteField;
    float* floatData[6];

    for(auto i = 0; i<6; i++){
        floatData[i] = new float[size_x*size_y];
    }

    sprintf(filename, ".//Fields//Diag2D//Field2D%03ld",timestep / TimeStepDelayDiag2D);

    MPI_File_open(_world.MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fField2D);

    for( auto i = 0; i < size_x; i++ ){
      for( auto j = 0; j < size_y; j++ ){
          floatData[0][i*size_y + j] = float(fieldE(i,j).x() );
          floatData[1][i*size_y + j] = float(fieldE(i,j).y() );
          floatData[2][i*size_y + j] = float(fieldE(i,j).z() );
          floatData[3][i*size_y + j] = float(fieldB(i,j).x() );
          floatData[4][i*size_y + j] = float(fieldB(i,j).y() );
          floatData[5][i*size_y + j] = float(fieldB(i,j).z() );
      }
    }
    
    auto sumSize_x = _world.MPIconf.accum_sum_line(size_x);
    
    long startWrite = sumSize_x * size_y * sizeof(float) + 2 * sizeof(float);

    if(_world.MPIconf.is_last_line()){
        info = float(sumSize_x + size_x);
        MPI_File_write_at(fField2D, 0, &info, 1, MPI_FLOAT, &status);
        info = float(size_y);
        MPI_File_write_at(fField2D, sizeof(float), &info, 1, MPI_FLOAT, &status);
        sizeWriteField = (sumSize_x + size_x) * size_y * sizeof(float);
    }
    

    MPI_Bcast(&sizeWriteField, 1, MPI_LONG, _world.MPIconf.last_line(), _world.MPIconf.comm_line() );

    //int startWrite = _world.MPIconf.rank_line()*(PlasmaCellsX_glob/_world.MPIconf.size_line())*size_y*sizeof(float) + 2*sizeof(float);
    
    //if(!_world.MPIconf.is_first_line() ){ 
    //  startWrite += DampCellsX_glob[0] * size_y * sizeof(float);
    //}
//    int sizeWrite = (PlasmaCellsX_glob + DampCellsX_glob[0] + DampCellsX_glob[1])*size_y*sizeof(float);
    //int sizeWrite = (PlasmaCellsX_glob + DampCellsX_glob[0] + DampCellsX_glob[1])*size_y*sizeof(float);
    
    for(auto i = 0; i<6; ++i){
        MPI_File_write_at(fField2D, startWrite + sizeWriteField*i,floatData[i], sizeData, MPI_FLOAT, &status);
    }
    
    MPI_File_close(&fField2D);
    
    for(auto i = 0; i<6; i++){
        delete[] floatData[i];
    }

}*/
void Writer::write_fields2D(const Array2D<double3>& fieldE, const Array2D<double3>& fieldB, const Array2D<double3>& fieldJ,  const long& timestep){
    if (!_world.MPIconf.is_master_depth()) return;
    
    MPI_File fField2D;
    MPI_Status status;
    
    char filename[50];
    float info;
    long size_x = fieldE.size()(0) - ADD_NODES;
    long size_y = fieldE.size()(1);
    long sizeData = size_x *size_y;
    long sizeWriteField;
    float* floatData[9];

    for(auto i = 0; i<9; i++){
        floatData[i] = new float[size_x*size_y];
    }

    sprintf(filename, ".//Fields//Diag2D//Field2D%03ld",timestep / TimeStepDelayDiag2D);

    MPI_File_open(_world.MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fField2D);

    for( auto i = 0; i < size_x; i++ ){
      for( auto j = 0; j < size_y; j++ ){
          floatData[0][i*size_y + j] = float(fieldE(i,j).x() );
          floatData[1][i*size_y + j] = float(fieldE(i,j).y() );
          floatData[2][i*size_y + j] = float(fieldE(i,j).z() );
          floatData[3][i*size_y + j] = float(fieldB(i,j).x() );
          floatData[4][i*size_y + j] = float(fieldB(i,j).y() );
          floatData[5][i*size_y + j] = float(fieldB(i,j).z() );
          floatData[6][i*size_y + j] = float(fieldJ(i,j).x() );
          floatData[7][i*size_y + j] = float(fieldJ(i,j).y() );
          floatData[8][i*size_y + j] = float(fieldJ(i,j).z() );
      }
    }
    
    auto sumSize_x = _world.MPIconf.accum_sum_line(size_x);
    
    long startWrite = sumSize_x * size_y * sizeof(float) + 2 * sizeof(float);

    if(_world.MPIconf.is_last_line()){
        info = float(sumSize_x + size_x);
        MPI_File_write_at(fField2D, 0, &info, 1, MPI_FLOAT, &status);
        info = float(size_y);
        MPI_File_write_at(fField2D, sizeof(float), &info, 1, MPI_FLOAT, &status);
        sizeWriteField = (sumSize_x + size_x) * size_y * sizeof(float);
    }
    

    MPI_Bcast(&sizeWriteField, 1, MPI_LONG, _world.MPIconf.last_line(), _world.MPIconf.comm_line() );

    //int startWrite = _world.MPIconf.rank_line()*(PlasmaCellsX_glob/_world.MPIconf.size_line())*size_y*sizeof(float) + 2*sizeof(float);
    
    //if(!_world.MPIconf.is_first_line() ){ 
    //  startWrite += DampCellsX_glob[0] * size_y * sizeof(float);
    //}
//    int sizeWrite = (PlasmaCellsX_glob + DampCellsX_glob[0] + DampCellsX_glob[1])*size_y*sizeof(float);
    //int sizeWrite = (PlasmaCellsX_glob + DampCellsX_glob[0] + DampCellsX_glob[1])*size_y*sizeof(float);
    
    for(auto i = 0; i<9; ++i){
        MPI_File_write_at(fField2D, startWrite + sizeWriteField*i,floatData[i], sizeData, MPI_FLOAT, &status);
    }
    
    MPI_File_close(&fField2D);
    
    for(auto i = 0; i<9; i++){
        delete[] floatData[i];
    }

}
/*
void RWriter::Diag_Fields2D(const Array2D<double3>& fieldE, const Array2D<double3>& fieldB,  const long& timestep, const MPI_Topology& MPIconf){
    MPI_File fField2D;
    MPI_Status status;
    
    if (MPIconf.RankDepth != 0) return;

    char filename[100];
    float info;
    long nd1 = fieldE.size_d1();
    long nd2 = fieldE.size_d2();
    int sizeData = (nd1 - ADD_NODES) * nd2;
    static float* floatData[6];

    for(auto i = 0; i<6; i++)
        floatData[i] = new float[nd1*nd2];

    sprintf(filename, ".//Fields//Diag2D//Field2D%03ld",timestep / TimeDelay);

    MPI_File_open(MPIconf.CommLine, filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fField2D);

    for( auto i = 0; i < nd1; ++i ){
        for( auto j = 0; j < nd2; ++j ){
            floatData[0][i*nd2 + j] = float( fieldE(i,j).x() );
            floatData[1][i*nd2 + j] = float( fieldE(i,j).y() );
            floatData[2][i*nd2 + j] = float( fieldE(i,j).z() );
            floatData[3][i*nd2 + j] = float( fieldB(i,j).x() );
            floatData[4][i*nd2 + j] = float( fieldB(i,j).y() );
            floatData[5][i*nd2 + j] = float( fieldB(i,j).z() );
        }
    }
    
    if(MPIconf.RankLine == 0){
          info = float(PlasmaCellsX_glob + DampCellsX_glob[0] + DampCellsX_glob[1]);
          MPI_File_write_at(fField2D, 0, &info, 1, MPI_FLOAT, &status);
          info = float(nd2);
          MPI_File_write_at(fField2D, sizeof(float), &info, 1, MPI_FLOAT, &status);
    }
    
    int startWrite = MPIconf.RankLine * (PlasmaCellsX_glob / MPIconf.SizeLine) * nd2 * sizeof(float) + 2*sizeof(float);
    
    if(MPIconf.RankLine != 0) startWrite += DampCellsX_glob[0] * nd2 * sizeof(float);
        int sizeWrite = (PlasmaCellsX_glob + DampCellsX_glob[0] + DampCellsX_glob[1]) * nd2 * sizeof(float);
    
    for(auto i = 0; i<6; ++i)
        MPI_File_write_at(fField2D, startWrite + sizeWrite*i,floatData[i], sizeData, MPI_FLOAT, &status);
    
    MPI_File_close(&fField2D);

}
*/
