#ifndef DIAGNOSTIC_H_
#define DIAGNOSTIC_H_
#define DIAGNOSTIC_H_
#include "World.h"
#include "Mesh.h"
#include "Particles.h"
#include "Timer.h"
void write_array2D(const Array2D<double>& data, long size1, long size2, const char* filename, const MPI_Topology& MPIconf);



template <typename T>
struct BoundData{
    T x_min, x_max;
    T y_min, y_max;
    BoundData(){
        std::cout<< "Error! You need template specifications for this type!\n";
    };
    BoundData(const Region &region){
        std::cout<< "Error! You need template specifications for this type!\n";
    };
    BoundData(const Region &region, long n){
        std::cout<< "Error! You need template specifications for this type!\n";
    };
    ~BoundData(){};
    void clear(){
        x_min = 0.;
        x_max = 0.;
        y_min = 0.;
        y_max = 0.;
    };
};
template<>
inline BoundData<Array1D<double> >::BoundData(const Region &region): x_min(region.nd2),x_max(region.nd2),
                                                                     y_min(region.nd1),y_max(region.nd1) {
        clear();
}
template<>
inline BoundData<Array2D<double> >::BoundData(const Region &region, long n): x_min(region.nd2,n),x_max(region.nd2,n),
                                                                     y_min(region.nd1,n),y_max(region.nd1,n) {
        clear();
}
template<>
inline void BoundData<Array2D<double3> >::clear(){
        x_min = double3(0.,0.,0.);
        x_max = double3(0.,0.,0.);
        y_min = double3(0.,0.,0.);
        y_max = double3(0.,0.,0.);
}

template<>
inline BoundData<Array2D<double3> >::BoundData(const Region &region, long n):x_min(region.nd2,n),x_max(region.nd2,n),
                                                                     y_min(region.nd1,n),y_max(region.nd1,n) {
        clear();
}
template<>
inline BoundData<double>::BoundData(){
        clear();
};
template<>
inline BoundData<double3>::BoundData(){
        clear();
};
struct DiagDataParams{
    std::vector<double> sliceRadiationLineX, sliceRadiationLineY;
    std::vector<double2> zondCoords;
    std::vector<double> zondCoordsLineX,zondCoordsLineY;
};

struct DiagData{

    
    DiagData(const Region &region){//: powerRadLine(region),
                            //powerRadAvgLine(region,long(2*PI/Dt)),
                            //fieldEAvgLine(region,long(2*PI/Dt)),
                            //fieldBAvgLine(region,long(2*PI/Dt)){
                            //energyELine(region.nd1),energyBLine(region.nd1){

        std::vector< std::vector<std::string> > vecStringParams;
        read_params_to_string("Diagnostics","./Diagnostics.cfg",vecStringParams);

        for (const auto& line: vecStringParams[0]){
            set_params_from_string(line);
        }

        for( const auto& elem : params.sliceRadiationLineX){
            std::cout  << "slices on LineX created for coord " << elem <<"\n";
            powerRadLineX.emplace_back( Array1D<double>(region.nn.y() ) );
            powerRadAvgLineX.emplace_back( Array2D<double>(region.nn.y(),long(2*PI/Dt) ) );
            fieldEAvgLineX.emplace_back( Array2D<double3>(region.nn.y(),long(2*PI/Dt) ) );
            fieldBAvgLineX.emplace_back( Array2D<double3>(region.nn.y(),long(2*PI/Dt) ) );
        }     
        for( const auto& elem : params.sliceRadiationLineY){
            std::cout  << "slices on LineY created for coord " << elem <<"\n";
            powerRadLineY.emplace_back( Array1D<double>(region.nn.x() ) );
            powerRadAvgLineY.emplace_back( Array2D<double>(region.nn.x(),long(2*PI/Dt) )  );
            fieldEAvgLineY.emplace_back( Array2D<double3>(region.nn.x(),long(2*PI/Dt) ) );
            fieldBAvgLineY.emplace_back( Array2D<double3>(region.nn.x(),long(2*PI/Dt) ) );
        }     
         //powerRadX = {0.,0.};
         //powerRadY = {0.,0.};
         //powerRadAvgX = {0.,0.};
         //powerRadAvgY = {0.,0.};
         //powerRadHalfY = {0.,0.};
         //powerRadAvgHalfY = {0.,0.};
        for(auto& data : powerRadAvgLineX){
            data.clear();
        }
        for(auto& data : powerRadAvgLineY){
            data.clear();
        }
        for(auto& data : fieldEAvgLineX){
            data.clear();
        }
        for(auto& data : fieldEAvgLineY){
            data.clear();
        }
        for(auto& data : fieldBAvgLineX){
            data.clear();
        }
        for(auto& data : fieldBAvgLineY){
            data.clear();
        }
       clear() ;

    };

    void set_params_from_string(const std::string& line);

    void calc_energy(const Mesh &mesh,const std::vector<ParticlesArray> &species);
    void calc_radiation_Pointing(const Mesh &mesh, const Region &region);
    void calc_fields_avg_line(const Mesh& mesh,const World& world,long timestep);
    void calc_radiation_Pointing_avg(const Mesh &mesh,const Region &region,long timestep);

    void calc_energy_half(const Mesh &mesh,const std::vector<ParticlesArray> &species);
    void calc_radiation_Pointing_half(const Mesh &mesh, const Region &region);
    void calc_radiation_Pointing_avg_half(const Mesh &mesh,const Region &region,long timestep);

    
    void clear(){
        
        for(auto& data : powerRadLineX){
            data.clear();
        }
        for(auto& data : powerRadLineY){
            data.clear();
        }
        //for(auto& data : powerRadAvgLineX){
         //   data.clear();
       // }
       // for(auto& data : powerRadAvgLineY){
        //    data.clear();
       // }
        //for(auto& data : fieldEAvgLineX){
        //    data.clear();
        //}
        //for(auto& data : fieldEAvgLineY){
        //    data.clear();
       // }
       // for(auto& data : fieldBAvgLineX){
        //    data.clear();
       // }
       // for(auto& data : fieldBAvgLineY){
        //    data.clear();
        //}
         powerRadX = {0.,0.};
         powerRadY = {0.,0.};
         powerRadAvgX = {0.,0.};
         powerRadAvgY = {0.,0.};
         powerRadHalfY = {0.,0.};
         powerRadAvgHalfY = {0.,0.};
    };


    BoundData<double> powerRad;
    BoundData<double> powerRadAvg;

    //BoundData<double> powerRadHalf;
    //BoundData<double> powerRadAvgHalf;
    //BoundData< Array1D<double> > powerRadLine;
    //BoundData< Array2D<double> > powerRadAvgLine;
    //BoundData< Array2D<double3> > fieldEAvgLine;
    //BoundData< Array2D<double3> > fieldBAvgLine;

    std::vector< Array1D<double> > powerRadLineX;
    std::vector< Array1D<double> > powerRadLineY;
    std::vector< Array2D<double> > powerRadAvgLineX;
    std::vector< Array2D<double> > powerRadAvgLineY;
    std::vector< Array2D<double3> > fieldEAvgLineX;
    std::vector< Array2D<double3> > fieldBAvgLineX;
    std::vector< Array2D<double3> > fieldEAvgLineY;
    std::vector< Array2D<double3> > fieldBAvgLineY;
    std::vector< double >  powerRadX,powerRadY,powerRadAvgX,powerRadAvgY;
    std::vector< double >  powerRadHalfY, powerRadAvgHalfY;
    //Array1D<double> energyELine,energyBLine;

    std::map<std::string,double> energyParticlesKinetic, energyParticlesKineticHalf;
    std::map<std::string,double> energyParticlesInject;
    std::map<std::string,double> energyParticlesInjectPrev;
    double energyFieldE, energyFieldB;
    double energyFieldEHalf, energyFieldBHalf;
    DiagDataParams params;

};




struct Writer{
protected:
    const World &_world;
    const Mesh &_mesh;
    const std::vector<ParticlesArray> &_species;
public:
    FILE *fDiagEnergies, *fDiagEnergiesHalf;
    DiagData diagData;

    Writer(const World &world, const Mesh &mesh,std::vector<ParticlesArray> &species);

    void output( long timestep);
    ~Writer(){
        if( !_world.MPIconf.is_master() ) return;
            fclose(fDiagEnergies);  
            fclose(fDiagEnergiesHalf);  
    } 

    void write_particles2D(long timestep);
    void write_energies(long timestep);
    void write_energies_half(long timestep);
    void write_radiation_line(long timestep);
    void write_radiation_avg_line(long timestep);
    void write_line_dataX(const Array1D<double>& array, long sizeData,const std::string& fname, MPI_File& mfile ,const MPI_Topology &MPIconf , long currentStep);
    void write_line_dataY(const Array1D<double>& array, long sizeData, const std::string& fname, MPI_File& mfile ,const MPI_Topology &MPIconf , long currentStep);


    void diag_zond(long timestep);
    void diag_zond_lineX_bin(const Array2D<double3>& fieldE, const Array2D<double3>& fieldB,long timestep);
    void diag_zond_lineY_bin(const Array2D<double3>& fieldE, const Array2D<double3>& fieldB, long timestep);
    void write_fields2D(const Array2D<double3>& fieldE, const Array2D<double3>& fieldB,  const long& timestep);
    void write_fields2D(const Array2D<double3>& fieldE, const Array2D<double3>& fieldB, const Array2D<double3>& fieldJ,  const long& timestep);

};
#endif 	
