//#include "World.h"
#include "Vec.h"
#include "SolverPoisson.h"
#include <cmath>

void exchange_phi(Array2D<double>& phi, const MPI_Topology &MPIconf){

  int tag = 11;
  MPI_Status status;
  const long2 size = phi.size();

  const long bufSize = size.y(); 
  long sendIndx, recvIndx; 
  static double *recvBuf = new double[bufSize];
  long shift = 2 * SHAPE_SIZE - 1;
  
  auto right = MPIconf.next_line();
  auto left = MPIconf.prev_line();

  sendIndx = (size.x()-shift)*size.y();
  // fromRight to Left
  
  MPI_Sendrecv(&phi.data(sendIndx), bufSize, MPI_DOUBLE_PRECISION, right, tag, 
                           recvBuf, bufSize, MPI_DOUBLE_PRECISION, left, tag, 
                           MPIconf.comm_line(), &status);
  if( !MPIconf.is_first_line() ){
      for (auto i = 0; i < bufSize; i++){
        phi.data(i) = recvBuf[i];
      }
  }
  
  sendIndx = (shift-1)*size.y();
  MPI_Sendrecv(&phi.data(sendIndx), bufSize, MPI_DOUBLE_PRECISION,left , tag, 
                           recvBuf, bufSize, MPI_DOUBLE_PRECISION, right, tag, 
                           MPIconf.comm_line(), &status);
  
  recvIndx = (size.x()-1)*size.y();  
     
  if( !MPIconf.is_last_line() ){

      for (auto i = 0; i < bufSize; i++){
        phi.data(recvIndx+i) = recvBuf[i];
      }
  }
  
  
  MPI_Barrier(MPIconf.comm_line() );
  
}

void Gauss_Zeidel(const Array2D<double>& rho, Array2D<double>& phi,double& err, int color,double dx){

 long i,j;
    double value,value1;
    //double Dx = dx;
    //double Dy = dx;

    //const double rdx = 1. / Dx;
    //const double rdy = 1. / Dy;
    const long size_x = rho.size().x();
    const long size_y = rho.size().y();
        double w=1.1 ;
    //double w = 1.+2*((0.5*PI*Dr*Dr/size_z)*(0.5*PI*Dr*Dr/size_z) + (0.5*PI*Dz*Dz/size_r)*(0.5*PI*Dz*Dz/size_r))/(Dr*Dr+Dz*Dz);
    double k = 0.5/(Dx*Dx+Dy*Dy); 
    //double k0 = (4*Dz*Dz+2*Dr*Dr) / (Dr*Dr*Dz*Dz);
	
	//std::cout<<"w = "<< w  << " "<< ((0.5*PI*Dr*Dr/size_z)*(0.5*PI*Dr*Dr/size_z) + (0.5*PI*Dz*Dz/size_r)*(0.5*PI*Dz*Dz/size_r))/(Dr*Dr+Dz*Dz) <<"\n";
	
	    for(i = 1; i < size_x-1; ++i){
			for(j = 1; j < size_y-1; ++j){
				if ( (i+j)%2 != color) continue;
			    value = k*(  Dx * Dx * ( phi(i,j-1) + phi(i,j+1) )
			    		   + Dy * Dy * ( phi(i-1,j) + phi(i+1,j)) + Dx*Dx*Dy*Dy*rho(i,j)  );
			    value1 = (1-w) * phi(i,j) + w*value;
			    if(value1 >1e-7) 
			    	err = std::max( fabs((value1-phi(i,j))/value1), err );
			    phi(i,j)=value1;

			}
	    }

}

void solver_Poisson(const Array2D<double>& rho, Array2D<double>& phi, const World& world,double dx){
    double eps = 4.e-4;
    long i=0;
    double err=0;

    double maxerr=1.e9;
    //double maxerr_prev = maxerr;
    int color = 0;
    while (maxerr > eps ){//&& (maxerr < maxerr_prev || i<10) ) {
    	color =0;
    	err = 0;
    	i++;
	   //maxerr_prev = maxerr;
	   Gauss_Zeidel(rho,phi,err,color,dx);
	   exchange_phi(phi,world.MPIconf);
	   //get phi(0),phi(nx-1) - color ==0
	   color = 1;// - color%2;
	   Gauss_Zeidel(rho,phi,err,color,dx);
	   exchange_phi(phi,world.MPIconf);
	   // get phi(0),phi(n-1) - color ==1
    	MPI_Allreduce ( MPI_IN_PLACE, &err, 1, MPI_DOUBLE, MPI_MAX, world.MPIconf.comm_line());

		maxerr = err;
		std::cout << "Error = "<< maxerr << "\n";
	}		
	std::cout<<" iterations = " << i << "\n";
} 

