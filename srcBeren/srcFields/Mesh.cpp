#include "Mesh.h"
#include "World.h"
#include "Shape.h"
#include "SolverFDTD.h"
#include "SolverFDTD_PML.h"
#include "Damping.h"
#include <cmath>

double u_exact ( double x, double y )

{
  double pi = 3.141592653589793;
  double value;

  value = sin ( pi * x * y );

  return value;
}
//****************************************************************************80

double uxxyy_exact ( double x, double y )

{
  double pi = 3.141592653589793;
  double value;

  value = - pi * pi * ( x * x + y * y ) * sin ( pi * x * y );

  return value;
}

void Mesh::get_rho(const Array2D<double>& source){
  const long2 size = rho.size();
    for( auto i = 0; i < size.x(); ++i){
      for( auto j = 0; j < size.y(); ++j){
        rho(i,j)+= source(i,j);
    }
  }
}

Mesh::Mesh(const World& world) : 
          fieldE(world.region.nn),fieldB(world.region.nn),
          fieldEp(world.region.nn),fieldBp(world.region.nn),
          fieldJ(world.region.nn),fieldB0(world.region.nn),
          phi(world.region.nn), rho(world.region.nn),_world(world){
    
    if (RECOVERY){ 
      read_from_recovery(world.MPIconf);
    } 
    else{
      set_fields();
    }

    std::vector< std::vector<std::string> > stringParams;
    read_params_to_string("Lasers","./LasParams.cfg",stringParams);
    for( const auto &params  : stringParams){
        lasers.emplace_back(params);
    }
}

void Mesh::set_fields(){
	set_uniform_fields();
 // test_poisson();
    fieldB0 = fieldB;

} 

void rhs (Array2D<double>& f )

{
  //double fnorm;
  long i;
  long j;
  long nx = f.size().x();
  long ny = f.size().y();
  double x;
  double y;
//

  for ( j = 0; j < ny; j++ )
  {
    y = ( double ) ( j ) / ( double ) ( ny - 1 );
    for ( i = 0; i < nx; i++ )
    {
      x = ( double ) ( i ) / ( double ) ( nx - 1 );
      if ( i == 0 || i == nx - 1 || j == 0 || j == ny - 1 )
      {
        f(i,j) = u_exact ( x, y );
      }
      else
      {
        f(i,j) = - uxxyy_exact ( x, y );
      }
    }
  }

  return;
}

void Mesh::test_poisson(){
  const long2 size = fieldE.size();
  //long sizex = _world.region.numCells_d1;
  double dx = 1.0 / ((NumCellsX_glob+ ADD_NODES-1)*Dx);
  double dy = 1.0 / ((NumCellsX_glob+ ADD_NODES-1)*Dy);
  Array2D<double> rho(size),uexact(size);
  rhs ( rho );

    for( auto i = 0; i < size.x(); ++i){
    for( auto j = 0; j < size.y(); ++j){
      if (i==0 || i == size.x() -1 || j == 0 || j == size.y()-1 ) {
        phi(i,j) = rho(i,j); //u_exact(i/double(size.x()-1),j/double(size.y()-1) );
        //rho(i,j)=0.; 
      }
      else{
        phi(i,j)=0.;
        //exact(i,j) = u_exact(i/double(size.x()-1),j/double(size.y()-1) );
        //rho(i,j) = - uxxyy_exact(i/double(size.x()-1),j/double(size.y()-1) ); 
      }
    }
  }
    for ( auto j = 0; j < size.y(); j++ )
  {
    auto y = ( double ) ( j ) / ( double ) ( size.y() - 1 );
    for ( auto i = 0; i < size.x(); i++ )
    {
      auto x = ( double ) ( i ) / ( double ) ( size.x() - 1 );
      uexact(i,j) = u_exact ( x, y );
    }
  }

  solver_Poisson(rho, phi, _world,dx);

  for( auto i = 0; i < size.x(); ++i){
    for( auto j = 0; j < size.y(); ++j){
      fieldE(i,j).x() = uexact(i,j);
      fieldE(i,j).y() = phi(i,j);
      fieldE(i,j).z() = rho(i,j);
    }
  }
}

void Mesh::get_phi(){
  double dx = Dx;
  const long2 size = rho.size();
  solver_Poisson(rho, phi, _world,dx);

  for( auto i = 0; i < size.x(); ++i){
    for( auto j = 0; j < size.y(); ++j){
      fieldE(i,j).x() = 0;//uexact(i,j);
      fieldE(i,j).y() = phi(i,j);
      fieldE(i,j).z() = rho(i,j);
    }
  }
}
void Mesh::set_uniform_fields(){

  const long2 size = fieldE.size();
  for( auto i = 0; i < size.x(); ++i){
    for( auto j = 0; j < size.y(); ++j){
      fieldE(i,j) = 0;
      fieldB(i,j).x() = BUniform[0];
      fieldB(i,j).y() = BUniform[1];
      fieldB(i,j).z() = BUniform[2];
    }
  }
}
double Mesh::calc_energy_field(const Array2D<double3>& field) const{
  double potE = 0;
  const long2 size = field.size();

  for(auto i = 0; i < size.x(); ++i){
    for(auto j = 0; j < size.y(); ++j){
      potE += dot(field(i,j),field(i,j) );      
    }
  }
  potE *= (0.5 * Dx * Dy );
  return potE;
}
double Mesh::calc_energy_field_half(const Array2D<double3>& field) const{
  double potE = 0;
  const long2 size = field.size();

  for(auto i = 0; i < size.x(); ++i){
    double coordX = _world.region.get_coord_from_index(i);
    double2 coordGlob =  _world.region.get_coord_glob(double2(coordX,0.));
    if( coordGlob.x() > HalfPointCoordX ) continue;

    for(auto j = 0; j < size.y(); ++j){
      potE += dot(field(i,j),field(i,j) );      
    }
  }
  potE *= (0.5 * Dx * Dy );
  return potE;
}



void Mesh::reduce_current(const MPI_Topology &MPIconf){
  const long2 size = fieldJ.size();
  const long capacity = fieldJ.capacity();
  int left, right;
  int tag = 2;
  MPI_Status status;
  int bufSize = ADD_NODES * size.y();
  long sendIndx; 
  static double3 *recvBuf = new double3[bufSize];
  
  MPI_Allreduce ( MPI_IN_PLACE, &fieldJ.data(0), 3*capacity, MPI_DOUBLE, MPI_SUM, MPIconf.comm_depth() ) ;
  
  MPI_Barrier(MPI_COMM_WORLD);

  
  if (MPIconf.rank_line() < MPIconf.size_line() - 1)
    right = MPIconf.rank_line() + 1;
  else
    right = 0;
  
  if (MPIconf.rank_line() > 0)
    left = MPIconf.rank_line() - 1;
  else
    left = MPIconf.size_line() - 1;


  sendIndx = capacity - bufSize;
  // fromRight to Left
  MPI_Sendrecv(&fieldJ.data(sendIndx), 3*bufSize, MPI_DOUBLE_PRECISION, right, tag, 
                           recvBuf, 3*bufSize, MPI_DOUBLE_PRECISION, left, tag, 
                           MPIconf.comm_line(), &status);
  
  for (long i = 0; i< bufSize; i++){
    fieldJ.data(i) += recvBuf[i];

  }
  
  MPI_Sendrecv(&fieldJ.data(0), 3*bufSize, MPI_DOUBLE_PRECISION,left , tag, 
                           &fieldJ.data(sendIndx), 3*bufSize, MPI_DOUBLE_PRECISION, right, tag, 
                           MPIconf.comm_line(), &status);

  
}

void exchange_fieldsE(Array2D<double3>& fieldE, const MPI_Topology &MPIconf){

  int tag = 1;
  MPI_Status status;
  const long2 size = fieldE.size();

  const long bufSize = size.y(); 
  long sendIndx, recvIndx; 
  static double3 *recvBuf = new double3[bufSize];
  long shift = 2 * SHAPE_SIZE - 1;
  
  auto right = MPIconf.next_line();
  auto left = MPIconf.prev_line();

  sendIndx = (size.x()-shift)*size.y();
  // fromRight to Left
  
  MPI_Sendrecv(&fieldE.data(sendIndx), 3*bufSize, MPI_DOUBLE_PRECISION, right, tag, 
                           recvBuf, 3*bufSize, MPI_DOUBLE_PRECISION, left, tag, 
                           MPIconf.comm_line(), &status);
  if( !MPIconf.is_first_line() ){
      for (auto i = 0; i < bufSize; i++){
        fieldE.data(i) = recvBuf[i];
      }
  }
  
  sendIndx = (shift-1)*size.y();
  MPI_Sendrecv(&fieldE.data(sendIndx), 3*bufSize, MPI_DOUBLE_PRECISION,left , tag, 
                           recvBuf, 3*bufSize, MPI_DOUBLE_PRECISION, right, tag, 
                           MPIconf.comm_line(), &status);
  
  recvIndx = (size.x()-1)*size.y();  
     
  if( !MPIconf.is_last_line() ){

      for (auto i = 0; i < bufSize; i++){
        fieldE.data(recvIndx+i) = recvBuf[i];
      }
  }
  
  
  MPI_Barrier(MPIconf.comm_line() );
  
}

double3 Mesh::get_fieldE_in_cell(long i, long j)  const{
  double3 E;
  E.x() = 0.5 * (fieldE(i,j).x() + fieldE(i,j+1).x() );
  E.y() = 0.5 * (fieldE(i,j).y() + fieldE(i+1,j).y() );
  E.z() = 0.25 * (fieldE(i,j).z() + fieldE(i+1,j).z() + fieldE(i,j+1).z() + fieldE(i+1,j+1).z() );
  return E;
}
double3 Mesh::get_fieldB_in_cell(long i, long j)  const{
  double3 B;
  B.x() = 0.5 * (fieldB(i,j).x() + fieldB(i+1,j).x() );
  B.y() = 0.5 * (fieldB(i,j).y() + fieldB(i,j+1).y() );
  B.z() = fieldB(i,j).z();
  return B;
}
inline double Shape2(const double& dist){
  double d = fabs(dist);
  if ( d <= 0.5 ) return (0.75 - d * d);
  else 
      if (d < 1.5)  return ((d - 1.5) * (d-1.5) * 0.5);
      else return 0.;
}

/*double3 Mesh::get_fieldE_in_pos(const double2& POS)  const{
  constexpr auto SMAX = 4; //2*SHAPE_SIZE;
  alignas(64) double sx[SMAX];
  alignas(64) double sy[SMAX];
  alignas(64) double sdx[SMAX];
  alignas(64) double sdy[SMAX];
  alignas(64) double3 E;
  double xx, yy, arg;
  double snm1, snm2, snm4;
  long xk, yk, indx, indy;
  
  xx = POS.x() / Dx;
  yy = POS.y() / Dy;
      
  xk = long(xx);
  yk = long(yy);
  
  for(auto n = 0; n < SMAX; ++n){
    arg = -xx + double(xk - CELLS_SHIFT + n);
    sx[n] = Shape2(arg);
    sdx[n] = Shape2(arg + 0.5);
    arg = -yy + double(yk - CELLS_SHIFT + n);
    sy[n] = Shape2(arg);
    sdy[n] = Shape2(arg + 0.5);
  }
    
  for( auto n = 0; n < SMAX; ++n){      
    for( auto m = 0; m < SMAX; ++m){

      snm1 = sdx[n] * sy[m];
      snm4 = sx[n] * sy[m];
      snm2 = sx[n] * sdy[m];
      indx = xk + n;
      indy = yk - CELLS_SHIFT + m;

      E += double3(snm1,snm2,snm4) * fieldE(indx,indy);

    }
  }
  
    return E;
}*/

double3 Mesh::get_fieldE_in_pos(const double2& r)  const{
  constexpr auto SMAX = 2*SHAPE_SIZE;
  alignas(64) double sx[SMAX];
  alignas(64) double sy[SMAX];
  alignas(64) double sdx[SMAX];
  alignas(64) double sdy[SMAX];
  alignas(64) double3 ep;
  alignas(64) double xx, yy, arg;
  alignas(64) double snm1, snm2, snm4;
  long xk, yk, indx, indy;
  ep = 0.;
  
  xx = r.x() / Dx;
  yy = r.y() / Dy;
      
  xk = long(xx);
  yk = long(yy);
  
  for(auto n = 0; n < SMAX; ++n){
    arg = -xx + double(xk - CELLS_SHIFT + n);
    sx[n] = Shape(arg);
    sdx[n] = Shape(arg + 0.5);
    arg = -yy + double(yk - CELLS_SHIFT + n);
    sy[n] = Shape(arg);
    sdy[n] = Shape(arg + 0.5);
  }
    
    for( auto n = 0; n < SMAX; ++n){      
    for( auto m = 0; m < SMAX; ++m){

      
      snm1 = sdx[n] * sy[m];
      snm4 = sx[n] * sy[m];
      snm2 = sx[n] * sdy[m];
      indx = xk + n;
      indy = yk - CELLS_SHIFT + m;
        
        ep += double3(snm1,snm2,snm4) * fieldE(indx,indy);

    }
  }
  
    return ep;
}


void Mesh::update(long timestep){
      const long2 size = fieldE.size();
 // for( auto i = 0; i < size.x(); ++i){
   // for( auto j = 0; j < size.y(); ++j){
     // if(fieldJ(i,j).x() != 0)
     //   std::cout << "j = " << i << " " << j << " "<< fieldJ(i,j).x() <<"\n";
   // }
  //}

    fieldB -= fieldB0;
    laser_source(timestep);
    
    #if DAMP_FIELDS == DAMP
      solver_FDTD(fieldE, fieldB, fieldJ, _world);
      damping_fields(fieldE, fieldB,_world.region);

    #elif DAMP_FIELDS == PML
      solver_FDTD_PML(fieldE, fieldB,fieldEp, fieldBp, fieldJ, _world);
    #endif
    
    fieldB += fieldB0;

}


void Mesh::laser_source(long timestep){
      /// Laser
    const auto l_min = _world.region.dampCells_d2;
    const auto l_max = _world.region.numCells_d2 - _world.region.dampCells_d2 ;
    
    for ( const auto& las : lasers){

      if ( !_world.region.in_region(las.start_d1) ) continue;
          	                 // std::cout <<"dfvd";

      if ( !las.is_work(timestep) )  continue;

      const long indx = round( (las.start_d1 - _world.region.origin) / Dx) + CELLS_SHIFT + 1;
      
      for (auto l = l_min; l <= l_max ; ++l){
        double y = (l+0.5)*Dy;

        if(las.type=="Ey"){
            fieldE(indx,l).y() = las.get_Ey(las.start_d1, y, timestep);
            fieldB(indx,l).z() = sign(las.vg)*las.get_Ey(las.start_d1 + 0.5*Dx,y-0.5*Dy, timestep);
        } 
        else if(las.type=="Ez"){
            fieldE(indx,l).z() = las.get_Ez(y-0.5*Dy, timestep*Dt-las.delay);
            fieldB(indx,l).y() = -sign(las.vg)*las.get_Ez(y-0.5*Dy, Dt*timestep-las.delay);  
        }
      }

    
    } 
  
}
