#ifndef MESH_H_
#define MESH_H_
#include "World.h"
#include "Laser.h"
#include <mpi.h>
#include <map>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <sys/types.h>
#include <sys/stat.h>
#include <assert.h>
#include "SolverPoisson.h"

void exchange_fieldsE(Array2D<double3>& field,const MPI_Topology &MPIconf);

struct Mesh{
    Mesh(const World& world);

    Array2D<double3> fieldE;
    Array2D<double3> fieldB;
    Array2D<double3> fieldEp;
    Array2D<double3> fieldBp;
    Array2D<double3> fieldJ;
    Array2D<double3> fieldB0;
    Array2D<double> phi;
    Array2D<double> rho;
    std::vector<Laser> lasers;
    
    void set_fields();
    void get_rho(const Array2D<double>& source);
    void get_phi();
    void test_poisson();

    void set_uniform_fields();
    void read_from_recovery(const MPI_Topology &MPIconf);
    void write_recovery(const MPI_Topology &MPIconf) const;
    void update(long timestep);
    double3 get_fieldE_in_pos(const double2& r) const;
    double3 get_fieldE_in_cell(long i, long j)  const;
    double3 get_fieldB_in_cell(long i, long j)  const;
    double calc_energy_field(const Array2D<double3>& field) const;
    double calc_energy_field_half(const Array2D<double3>& field) const;

    double get_fieldE_energy() const{
        return calc_energy_field(fieldE);
    };
    void laser_source(long timestep);
    double get_fieldB_energy() const{
        return calc_energy_field(fieldB);
    };
    double get_fieldB0_energy() const{
        return calc_energy_field(fieldB0);
    };    
    double get_fieldE_energy_half() const{
        return calc_energy_field_half(fieldE);
    };
    double get_fieldB_energy_half() const{
        return calc_energy_field_half(fieldB);
    };
    double get_fieldB0_energy_half() const{
        return calc_energy_field_half(fieldB0);
    };    

    void reduce_current(const MPI_Topology &MPIconf);
    ~Mesh(){
    }
protected:
    const World &_world;
};


#endif 
