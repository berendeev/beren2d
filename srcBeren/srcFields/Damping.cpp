#include "World.h"
#include "Damping.h"

/*void SetEnergyDampLineNull(const Region& domain){
    for( auto i = 0; i < NumCellsR_glob + 2 * SHAPE_SIZE - 1; ++i){
	energy.eDampLineB_Z1[i] = 0.;
	energy.eDampLineE_Z1[i] = 0.;
	energy.eDampLineB_Z2[i] = 0.;
	energy.eDampLineE_Z2[i] = 0.;
    }
    for( auto i = 0; i < NumCellsZ_glob + 2 * SHAPE_SIZE - 1; ++i){
	energy.eDampLineB_R[i] = 0.;
	energy.eDampLineE_R[i] = 0.;
    }
}*/

void damping_fields(Array2D<double3>& fieldE, Array2D<double3>& fieldB,const Region& domain){
    long i,j,i1,j1;
    double energyDamp, DampCells;
    const long cellsYP1 = 0.5 * (domain.numCells_d2 - 1.1*PlasmaCellsY_glob);
    const long cellsYP2 = 0.5 * (domain.numCells_d2 + 1.1*PlasmaCellsY_glob);
    long DampCellsX[2], DampCellsForceX[2];
    long DampCellsY = domain.dampCells_d2;
    long max_indx = domain.nd1-1;
    long max_indy = domain.nd2-1;
    DampCellsX[0] = domain.dampCells_d1[0] + CELLS_SHIFT + 1;
    DampCellsX[1] = domain.dampCells_d1[1] + CELLS_SHIFT + 1;
    DampCellsForceX[0] = domain.dampCellsForce_d1[0];
    DampCellsForceX[1] = domain.dampCellsForce_d1[1];
  
    if (domain.boundType_d1[0] == OPEN) { 
      	for(i = 0; i < DampCellsX[0]; i++){
	  		for (j = 0; j <= max_indy ; j++){

				if( ( i >= DampCellsX[0] - DampCellsForceX[0]) && j > cellsYP1 && j <cellsYP2 ){
				    i1 = i - (DampCellsX[0] - DampCellsForceX[0]);
				    DampCells = DampCellsForceX[0];
				}
				else{ 
				    i1 = i;
				    DampCells = DampCellsX[0];
				}
				
				Damping_Func(fieldE(i,j).x(), i1, DampCells, energyDamp);					
			
				Damping_Func(fieldE(i,j).y(), i1, DampCells, energyDamp);
							
				Damping_Func(fieldE(i,j).z(), i1, DampCells, energyDamp);
							
				Damping_Func(fieldB(i,j).z(), i1, DampCells, energyDamp);

				Damping_Func(fieldB(i,j).y(), i1, DampCells, energyDamp);				
				
				Damping_Func(fieldB(i,j).x(), i1, DampCells, energyDamp);
	    	}
		}
    }

    if (domain.boundType_d1[1] == OPEN) { 
      	for(i = max_indx; i > max_indx - DampCellsX[1]; i--){
	  	    for (j = 0; j <= max_indy ; j++){

			    if((i <= max_indx - (DampCellsX[1] - DampCellsForceX[1])) && j > cellsYP1 && j < cellsYP2){
				    i1 = -i + (max_indx - (DampCellsX[1] - DampCellsForceX[1]));
				    DampCells = DampCellsForceX[1];
			    } else{ 
				    i1 = -(i - max_indx);
				    DampCells = DampCellsX[1];
			    }
	    
				
				Damping_Func(fieldE(i,j).x(), i1, DampCells, energyDamp);							

				Damping_Func(fieldE(i,j).y(), i1, DampCells, energyDamp);
							
				Damping_Func(fieldE(i,j).z(), i1, DampCells, energyDamp);
							
				Damping_Func(fieldB(i,j).z(), i1, DampCells, energyDamp);

				Damping_Func(fieldB(i,j).y(), i1, DampCells, energyDamp);				
				
				Damping_Func(fieldB(i,j).x(), i1, DampCells, energyDamp);
	    	}
		}
    }

			
    for( i = 0; i <= max_indx; ++i){			
		for( j = max_indy; j > max_indy - DampCellsY; --j){
						
		    j1 = - j + max_indy;
		    DampCells = DampCellsY;

		    Damping_Func(fieldE(i,j).x(), j1, DampCells, energyDamp);
							
		    Damping_Func(fieldE(i,j).y(), j1, DampCells, energyDamp);
							
		    Damping_Func(fieldE(i,j).z(), j1, DampCells, energyDamp);
							
		    Damping_Func(fieldB(i,j).z(), j1, DampCells, energyDamp);

		    Damping_Func(fieldB(i,j).y(), j1, DampCells, energyDamp);

		    Damping_Func(fieldB(i,j).x(), j1, DampCells, energyDamp);

		}
		for( j = 0; j< DampCellsY; ++j){
						
		    j1 = j;//- j + domain.numCells_d2+ADD_NODES;
		    DampCells = DampCellsY;

		    Damping_Func(fieldE(i,j).x(), j1, DampCells, energyDamp);
							
		    Damping_Func(fieldE(i,j).y(), j1, DampCells, energyDamp);
							
		    Damping_Func(fieldE(i,j).z(), j1, DampCells, energyDamp);
							
		    Damping_Func(fieldB(i,j).z(), j1, DampCells, energyDamp);

		    Damping_Func(fieldB(i,j).y(), j1, DampCells, energyDamp);

		    Damping_Func(fieldB(i,j).x(), j1, DampCells, energyDamp);

		}		
    }

  
}

void Damping_Func(double& source, long i, long maxi, double& energyDamp){
    double koeff=0.8;
    double a, damp;

    a = (1.0-koeff)/(maxi*maxi);
    damp = a * i * i + koeff;

    energyDamp = 0.5*source*source*(1.0 - damp*damp);
    source *= damp;	
} 
