#include "World.h"
#include "Mesh.h"
#include "SolverFDTD.h"

static void update_fieldsB(const Array2D<double3>& fieldE, Array2D<double3>& fieldB){
    long i, j;
    const double dtp = 0.5 * Dt;
    const double rdx = 1. / Dx;
    const double rdy = 1. / Dy;
    const auto size = fieldB.size();
    const long size_d1 = size.x();
    const long size_d2 = size.y();

    for( i = 0; i < size_d1 - 1; ++i ){
		for( j = 0; j < size_d2 - 1; ++j ){
			fieldB(i,j).x() += ( - dtp * rdy * (fieldE(i,j+1).z() - fieldE(i,j).z() ) );
			fieldB(i,j).y() += dtp * rdx * (fieldE(i+1,j).z() - fieldE(i,j).z() );
			fieldB(i,j).z() += dtp * (rdy * (fieldE(i,j+1).x() - fieldE(i,j).x() ) - rdx * (fieldE(i+1,j).y() - fieldE(i,j).y() ) );
		}
    }

    i = size_d1 - 1;
    for( j = 0; j < size_d2 - 1; ++j){
		fieldB(i,j).x() += ( - dtp * rdy * (fieldE(i,j+1).z() - fieldE(i,j).z() ) );
    }

	j = size_d2 - 1;
    for(i = 0; i < size_d1 - 1; ++i){
		fieldB(i,j).y() += dtp * rdx * (fieldE(i+1,j).z() - fieldE(i,j).z() );
    }
}


void solver_FDTD(Array2D<double3>& fieldE, Array2D<double3>& fieldB, const Array2D<double3>& fieldJ, const World& world){
    long i, j;
    const double rdx = 1. / Dx;
    const double rdy = 1. / Dy;
    const auto size = fieldE.size();
    const long size_d1 = size.x();
    const long size_d2 = size.y();
    static Array2D<double> Ez0(2,size_d2);
    static Array2D<double> Ey0(2,size_d2);

    for (j = 0; j < size_d2; ++j){
		Ez0(0,j) = fieldE(1,j).z();
		Ez0(1,j) = fieldE(size_d1-2,j).z();	
		Ey0(0,j) = fieldE(1,j).y();
		Ey0(1,j) = fieldE(size_d1-2,j).y();      
    }

    update_fieldsB(fieldE, fieldB);
	
    i = 0;
    for(j = 1; j < size_d2; ++j){
	    fieldE(i,j).x() += ( Dt * rdy * (fieldB(i,j).z() - fieldB(i,j-1).z() ) - Dt * fieldJ(i,j).x());
    }
	
    for(i = 1; i < size_d1 - 1; ++i){
		for(j = 1; j < size_d2; ++j){
			    fieldE(i,j).x() += ( Dt * rdy * (fieldB(i,j).z() - fieldB(i,j-1).z() ) - Dt * fieldJ(i,j).x() );
			    fieldE(i,j).y() += (- Dt * rdx * (fieldB(i,j).z() - fieldB(i-1,j).z() ) - Dt * fieldJ(i,j).y() );
			    fieldE(i,j).z() += Dt*( rdx * (fieldB(i,j).y() - fieldB(i-1,j).y() ) - rdy * (fieldB(i,j).x() - fieldB(i,j-1).x() ) - fieldJ(i,j).z() );
		}
    }
    exchange_fieldsE(fieldE,world.MPIconf);
	
    if(world.region.boundType_d1[0] == OPEN ){
		for(j = 0; j < size_d2; ++j){
		    double Kabc = (Dt - Dx) / (Dt + Dx);
		    fieldE(0,j).z() = Ez0(0,j) + Kabc * (fieldE(1,j).z() - fieldE(0,j).z() );
		    fieldE(0,j).y() = Ey0(0,j) + Kabc * (fieldE(1,j).y() - fieldE(0,j).y() );
	  	}
    }
    
    if(world.region.boundType_d1[1] == OPEN){
	  for(j = 0; j < size_d2; ++j){
	    double Kabc = (Dt - Dx) / (Dt + Dx);
	    fieldE(size_d1-1,j).z() = Ez0(1,j) + Kabc * (fieldE(size_d1-2,j).z() - fieldE(size_d1 - 1,j).z() );
	    fieldE(size_d1-1,j).y() = Ey0(1,j) + Kabc * (fieldE(size_d1-2,j).y() - fieldE(size_d1 - 1,j).y() );
	  }

    }

   update_fieldsB(fieldE, fieldB);
			
} 

