#ifndef SOLVERPOISSON_H_
#define SOLVERPOISSON_H_
#include "World.h"
void solver_Poisson(const Array2D<double>& rho, Array2D<double>& phi, const World& world, double dx);

#endif 	
