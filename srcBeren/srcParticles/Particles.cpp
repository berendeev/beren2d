#include "Vec.h"
#include "World.h"
#include "Particles.h"
#include "Shape.h"
std::ostream& operator<<(std::ostream& out, const ParticleSimple& particle){
	out << particle.coord.x() << " " << particle.coord.y() << " " << particle.pulse.x() << " " << particle.pulse.y() << " " << particle.pulse.z();
	return out;
} 
std::ostream& operator<<(std::ostream& out, const ParticleMass& particle){
	out << particle.coord.x() << " " << particle.coord.y() << " " << particle.pulse.x() << " " << particle.pulse.y() << " " << particle.pulse.z() << " " << particle.mass;
	return out;
} 


std::ostream& operator<<(std::ostream& out,const double3& val){
	  out << " x " << val.x() << " y " << val.y() << " z " << val.z();
	  return out;
} 

long ParticlesArray::counter;
ParticlesArray::ParticlesArray(const std::vector<std::string>& vecStringParams, World& world):
    particlesData(world.region.nn),
    densityOnGrid(world.region.nn), phaseOnGrid(PxMax,PpMax), countInCell(world.region.nn),
    _world(world) {
    for (const auto& line: vecStringParams){
        set_params_from_string(line);
    }
    injectionEnergy = 0.;

    if (RECOVERY){ 
       // read_from_recovery(world.MPIconf);
    }else{
        set_distribution();
    }
    update_count_in_cell();
    density_on_grid_update();
    phase_on_grid_update(); 

}

double PulseFromKev(double kev, double mass){
  double gama = kev / MC2 + mass;
  return sqrt((gama*gama)- mass);
}


int get_num_of_type_particles(const std::vector<ParticlesArray> &Particles, const std::string& ParticlesType){
  for (int i = 0; i < NumOfPartSpecies; ++i){
    if(Particles[i].name == ParticlesType) return i;
  }
  return -1;
}

void join_density_comm_line(Array2D<double>& dens, const MPI_Topology& MPIconf){

  int tag = 1;
  MPI_Status status;
  long2 size = dens.size();
  int bufSize = ADD_NODES*size.y();
  long sendIndx; 
  static double *recvBuf = new double[bufSize];
  
   auto right = MPIconf.next_line();
  
    auto left = MPIconf.prev_line();


  sendIndx = (size.x()-ADD_NODES)*size.y();
  // fromRight to Left
  MPI_Sendrecv(&dens.data(sendIndx), bufSize, MPI_DOUBLE_PRECISION, right, tag, 
                           recvBuf, bufSize, MPI_DOUBLE_PRECISION, left, tag, 
                           MPIconf.comm_line(), &status);
  
  for (long i = 0; i< bufSize; i++){
    dens.data(i) += recvBuf[i];
  }
  
  MPI_Sendrecv(&dens.data(0), bufSize, MPI_DOUBLE_PRECISION,left , tag, 
                           &dens.data(sendIndx), bufSize, MPI_DOUBLE_PRECISION, right, tag, 
                           MPIconf.comm_line(), &status);
  
  MPI_Barrier(MPIconf.comm_line());
  
  /*
  int left, right;
  int tag = 1;
  MPI_Status status;
  int bufSize = ADD_NODES * dens.size_d2();
  long sendIndx; 
  static double *recvBuf = new double[bufSize];
  
  if (MPIconf.RankLine < MPIconf.SizeLine - 1)
    right = MPIconf.RankLine + 1;
  else
    right = 0;
  
  if (MPIconf.RankLine > 0)
    left = MPIconf.RankLine - 1;
  else
    left = MPIconf.SizeLine - 1;

  sendIndx = (dens.size_d1()-ADD_NODES)*dens.size_d2();
  // fromRight to Left
  MPI_Sendrecv(&dens.data(sendIndx), bufSize, MPI_DOUBLE_PRECISION, right, tag, 
                           recvBuf, bufSize, MPI_DOUBLE_PRECISION, left, tag, 
                           MPIconf.CommLine, &status);
  
  for (long i = 0; i< bufSize; i++){
    dens.data(i) += recvBuf[i];
  }
  
  MPI_Sendrecv(&dens.data(0), bufSize, MPI_DOUBLE_PRECISION,left , tag, 
                           &dens.data(sendIndx), bufSize, MPI_DOUBLE_PRECISION, right, tag, 
                           MPIconf.CommLine, &status);
  
  MPI_Barrier(MPIconf.CommLine);
  
  //delete [] recvBuf;*/
}

void ParticlesArray::density_on_grid_update(){ 
    constexpr auto SMAX = 2*SHAPE_SIZE;
    long yk, xk, n,m;
    double sx[SMAX];
    double sy[SMAX];
    double arg;
    Particle particle;
    long indx, indy;
    long2 size = densityOnGrid.size();
    //long capacity = densityOnGrid.capacity();
    
    densityOnGrid.clear();
    for ( long i = 0; i < particlesData.size().x()-SMAX+1; ++i){
    for ( long j = CELLS_SHIFT; j < particlesData.size().y()-SMAX+1+ CELLS_SHIFT; ++j){

    for ( ulong k = 0; k < particlesData(i,j).size(); ++k){

        particle = particlesData(i,j)(k);
        xk = long(particle.coord.x() / Dx);
        yk = long(particle.coord.y() / Dy);

        for(n = 0; n < SMAX; ++n){
            arg = -particle.coord.y() / Dy + double(yk - CELLS_SHIFT + n);
            sy[n] = Shape(arg) ;
            arg = -particle.coord.x() / Dx + double(xk - CELLS_SHIFT + n);
            sx[n] = Shape(arg);
        }

        for(n = 0; n < SMAX; ++n){
            indx = xk + n;
            for(m = 0; m < SMAX; ++m){
                indy = yk - CELLS_SHIFT + m; 
                densityOnGrid(indx,indy) += mpw(i,j,k) * sx[n] * sy[m] / (Dx*Dy);
            }
        }
    }
    }
    }

    MPI_Allreduce ( MPI_IN_PLACE, &densityOnGrid.data(0), size.x()*size.y(), MPI_DOUBLE, MPI_SUM, _world.MPIconf.comm_depth());
    MPI_Barrier(MPI_COMM_WORLD);

    join_density_comm_line(densityOnGrid, _world.MPIconf);

}

void ParticlesArray::phase_on_grid_update(){ 
            constexpr auto SMAX = 2*SHAPE_SIZE;
    long2 size = phaseOnGrid.size();
    long i, j, pk, xk;
    double x, px;
    bool blounder;
    Particle particle;
    double x_min = Dx*(_world.region.dampCells_d1[0]);
    double x_max = Dx*(_world.region.numCells_d1 - _world.region.dampCells_d1[1]);
    double pdx = (x_max - x_min) / PxMax;
    double pdp = (phasePXmax -phasePXmin) / PpMax;

    for(i = 0 ; i < size.x(); i++){
        for(j = 0; j < size.y(); j++){
            phaseOnGrid(i,j) = 0;
        }
    }
    for ( long i = 0; i < particlesData.size().x()-SMAX+1; ++i){
    for ( long j = CELLS_SHIFT; j < particlesData.size().y()-SMAX+1+ CELLS_SHIFT; ++j){

    for ( ulong k = 0; k < particlesData(i,j).size(); ++k){        
        particle = particlesData(i,j)(k);   
        x = particle.coord.x();
        px = particle.pulse.x();

        xk = long((x-x_min) / pdx); 
        pk = long((px - phasePXmin) / pdp);

        blounder = (xk<0) || (xk>=PxMax) || (pk<0) || (pk>=PpMax);
        if(!blounder){
            phaseOnGrid(xk,pk) += (mpw(i,j,k) / (Dx*Dy*pdx*pdp) );
        }

    }
    }
    }

    MPI_Allreduce ( MPI_IN_PLACE, &phaseOnGrid.data(0), size.x()*size.y(), MPI_DOUBLE, MPI_SUM, _world.MPIconf.comm_depth());
    MPI_Barrier(MPI_COMM_WORLD);

}


void ParticlesArray::set_distribution(){
		set_space_distribution();
		set_pulse_distribution();	
}

// Устанавливаем значения основных параметров в переменной Params в соответствии с содержимым сроки
void ParticlesArray::set_params_from_string(const std::string& line){
    std::vector<std::string> strvec;

    strvec = split(line, ' ');

    if(strvec[0]=="Particles") {
        this->name = strvec[1];
    }

    if(strvec[0]=="Temperature"){
        temperature = stod(strvec[1]);
    }

    if(strvec[0]=="Mass"){
        this->_mass = stod(strvec[1]);
    }

    if(strvec[0]=="Charge"){
       this->charge =  stol(strvec[1]);
    }
    if(strvec[0]=="SmoothMass"){
       option.smoothMass =  stol(strvec[1]);
    }
    if(strvec[0]=="SmoothMassSize"){
       option.smoothMassSize =  stod(strvec[1]);
    }
    if(strvec[0]=="SmoothMassMax"){
       option.smoothMassMax =  stod(strvec[1]);
    }  
    if(strvec[0]=="Width"){
        width = stod(strvec[1]);
    }
    if(strvec[0]=="Density"){
        this->density = stod(strvec[1]); 
        this->_mpw = density * Dx * Dy / double(NumPartPerCell);
    }
    if(strvec[0]=="Focus"){
       focus = stod(strvec[1]);
    }
    if(strvec[0]=="Velocity"){
        velocity = stod(strvec[1]);
    }
    if(strvec[0]=="Pot_I"){
        this->pot_I = stod(strvec[1]);
    }
    if(strvec[0]=="Pot_k"){
        this->pot_k = stod(strvec[1]);
    }

    if(strvec[0]=="Px_min"){
        this->phasePXmin = stod(strvec[1]);
    }

    if(strvec[0]=="Px_max"){
         this->phasePXmax = stod(strvec[1]);
    }
    if (strvec[0]=="DistParams"){
        if(strvec[1]=="UniformCosX_dn_k"){
            initDist = strvec[1];
            distParams.push_back(stod(strvec[2]));
            distParams.push_back(stod(strvec[3]));
        }
        if(strvec[1]=="Uniform"){
            initDist = strvec[1];
        }
        if(strvec[1]=="StrictUniform"){
            initDist = strvec[1];
        }
        if(strvec[1]=="None"){
           initDist = strvec[1];
        }
    }
    if(strvec[0]=="BoundResumption"){
        option.boundResumption = stod(strvec[1]);
    }
    if(strvec[0]=="SourceType"){
        option.sourceType = stod(strvec[1]);
    }
}



void ParticlesArray::update(Mesh& mesh,long timestep){
    
    //inject(timestep);
    move(mesh,timestep);
    move_virt(mesh,timestep);

    if (timestep % TimeStepDelayDiag2D == 0){
        density_on_grid_update();
        phase_on_grid_update();
    }
        
}
void ParticlesArray::inject(long timestep){
    switch(option.sourceType){
       case SOURCE_UNIFORM:
            //source_uniform_from_bound(timestep);
            break;
        case SOURCE_FOCUSED_GAUSS:
            //source_focused_gauss(timestep);
            break;
        case SOURCE_NONE:
            break;
        default:
            break;

        }
}

double ParticlesArray::get_kinetic_energy() const{
    double energy = 0;
    double3 pulse;
    double gama;
    for ( long i = 0; i < particlesData.size().x(); ++i){
    for ( long j = 0; j < particlesData.size().y(); ++j){

    for ( ulong k = 0; k < particlesData(i,j).size(); ++k){    
        pulse = particlesData(i,j)(k).pulse;
        gama = sqrt(mass(i,j,k)*mass(i,j,k) + dot(pulse,pulse) );
        energy += mpw(i,j,k) * (gama - mass(i,j,k));
    }   
}
}
    return energy;
}
double ParticlesArray::get_kinetic_energy_half() const{
    double energy = 0;
    double3 pulse;
    double2 coord;
    double gama;

    for ( long i = 0; i < particlesData.size().x(); ++i){
    for ( long j = 0; j < particlesData.size().y(); ++j){

    for ( ulong k = 0; k < particlesData(i,j).size(); ++k){    
            pulse = particlesData(i,j)(k).pulse;
        coord = particlesData(i,j)(k).coord;
        double2 coordGlob =  _world.region.get_coord_glob(coord);
        if( coordGlob.x() > HalfPointCoordX ) continue;
        
        gama = sqrt(mass(i,j,k)*mass(i,j,k) + dot(pulse,pulse) );
        energy += mpw(i,j,k) * (gama - mass(i,j,k));
    }   
    }
    }

    return energy;
}