#include "Particles.h"
/*
void ParticlesArray::source_uniform_from_bound(long timestep){
    double energyInj = 0;
	Particle particle;
	long i;
	double gama;
	double pz,x,y,px,py;
	double vb = velocity;
	long numInj = NumPartPerCell * width / Dy;
	double  particleMPW;
	bool LeaftInj =  (vb>0. && _world.region.boundType_d1[0] == OPEN);
	bool RightInj =  (vb<0. && _world.region.boundType_d1[1] == OPEN);

	if(!( LeaftInj || RightInj )) return;

	for(i = 0; i < numInj; ++i){

		x = Dx*Uniform01();

		
		bool CondInj = x <= Dt*fabs(vb);
		
		if( !CondInj ) continue;
			y =  0.5 * (Dy*NumCellsY_glob - width) + width*Uniform01();
				
			px = vb / sqrt(1.0-vb*vb);
			px += Gauss(temperature);
			py = Gauss(temperature);
			pz = Gauss(temperature);

				particle.coord.y() = y ;
				particle.pulse = double3(px,py,pz);
				
				if(LeaftInj ){
					  particle.coord.x() = x + Dx * _world.region.dampCells_d1[0];
				}
				else if (RightInj ){
					  particle.coord.x() = Dx * _world.region.numCells_d1 - x - Dx *  _world.region.dampCells_d1[1];
					}
					else{
					  std::cout << "Error Inject Particles type" << std::endl; exit(0); 
				}
				
			
				#ifdef PARTICLE_MASS
		                   particle.mass = _mass;
				#endif	
				#ifdef PARTICLE_MPW
		                   particle.mpw = particleMPW = std::min(1.,Dt*timestep/InjectSmoothTime) * density * Dx * Dy / (NumPartPerLine*NumPartPerLine);
;
				#else
						particleMPW = _mpw;
				#endif	
				gama = sqrt(_mass*_mass + dot(particle.pulse,particle.pulse) );
					
				energyInj += particleMPW*(gama - _mass);
				
				add_particle_scatter(particle);			
		
		}
			
	injectionEnergy += energyInj;
}

void ParticlesArray::source_focused_gauss(long timestep){
    double energyInj = 0;
    Particle particle;
	long i;//, kmax;
	double gama;
	double pz,x,y,px,py;
	double vb = velocity;
	double particleMPW;
	double sigma;

	long numInj = sqrt(2*PI)* NumPartPerCell*width/Dy;

	bool LeaftInj =  (vb>0. &&  _world.region.boundType_d1[0] == OPEN);
	bool RightInj =  (vb<0.  && _world.region.boundType_d1[1] == OPEN );

	if(!( LeaftInj || RightInj )) return;
	
	for(i = 0; i < numInj; ++i){

		x = Dx * Uniform01();

		y = 0.5*Dy*(_world.region.numCells_d2) + Gauss(width);

		px = vb / sqrt(1.0-vb*vb);

		sigma = (Rmax-width)/Lmax*px;
		
		py = Gauss(sigma/3.);
		pz = Gauss(sigma/3.);
		
//		r1 = sqrt((x + focus * px/pz)*(x + focus * px/pz)+(y + focus * py/pz)*(y + focus * py/pz));
		
		//x = x - focus * px/px;
		y = y - focus * py/px;
		
		//bool InFocus = rf < 3.*width;

		bool Bound1 = y > 1.2 * Dy * _world.region.dampCells_d2;
		bool Bound2 = y < Dy * (_world.region.numCells_d2 - 1.2 * _world.region.dampCells_d2);
		bool CondCurr = x <= Dt*fabs(px/sqrt(1. + px*px + py*py + pz * pz));
		
		bool CondInj = Bound1 && Bound2 && CondCurr; //&& InFocus;
		
		if( !CondInj ) continue;

				particle.coord.y() = y ;
					
				if(LeaftInj){
					  particle.coord.x() =  x + Dx *  _world.region.dampCells_d1[0];
				}
				else if ( RightInj){
					  particle.coord.x() = Dx * _world.region.numCells_d1 - x - Dx *  _world.region.dampCells_d1[1];
					  py = - py;
					  pz = - pz;
					}
					else{
					  std::cout << "Error Inject Particles type" << std::endl; 
					  exit(0); 
				}
				particle.pulse = double3(px,py,pz);

				#ifdef PARTICLE_MASS
		                   particle.mass = _mass;
				#endif	
				#ifdef PARTICLE_MPW
		                   particle.mpw = particleMPW = std::min(1.,Dt*timestep/InjectSmoothTime) * density * Dx * Dy / (NumPartPerLine*NumPartPerLine);
;
				#else
						particleMPW = _mpw;
				#endif	
				gama = sqrt(1. + dot(particle.pulse,particle.pulse) );
					
				energyInj += particleMPW*(gama - 1.);
				
				add_particle_scatter(particle);		
			
	}
			
	injectionEnergy += energyInj;
	//std::cout << "inject "<< injectionEnergy << "\n";

}*/
/*
void injectionFocused(ParticlesArray& particles ,  const World& world,\
            long timestep){
                double energyInj = 0;
        
	long i, kmax;
	long count;
	double pb,gama;
	double r,r1,r2,rf,z,pr, pp, pz,x,y,px,py;
	double vb = particles.velocity;
	double width = particles.width;
	double dens = particles.density;
	double zFocus = particles.focus;
	double sigma;
	long numInj = 4. / PI * NumPartPerCell * Rfocus / Dr;
	bool LeaftInj =  (particles.name == "BeamLeft" && world.MPIconf.RankLine == 0);
	bool RightInj =  (particles.name == "BeamRight" && world.MPIconf.RankLine == world.MPIconf.SizeLine -1 );

	if(!( LeaftInj || RightInj )) return;

	count = -1;
	kmax = particles.size();

	for(i = 0; i < numInj; ++i){

		z = Dz * Uniform01();

		x = width * Uniform01();
		y = width * Uniform01();
		
		rf = sqrt(x*x+y*y);
		
		pb = vb / sqrt(1.0-vb*vb);

		sigma = (Rmax - width) / Lmax * pb;
		
		px = Gauss(sigma/3.);
		py = Gauss(sigma/3.);

		pz = pb; 
		
		r1 = sqrt((x + zFocus * px/pz)*(x + zFocus * px/pz)+(y + zFocus * py/pz)*(y + zFocus * py/pz));

		x = x - zFocus * px/pz;
		y = y - zFocus * py/pz; 
		
		r2 = sqrt(x*x+y*y);
		r = r2;
		pr = x/r * px + y/r*py; 
		pp = - y/r * px + x/r*py; 
		
		bool InFocus = rf < width;
		bool Bound1 = r1 < 0.8*Dr*(NumCellsR_glob - DampCellsR_glob);
		bool Bound2 = r2 < 0.8*Dr*(NumCellsR_glob - DampCellsR_glob);
		bool CondCurr = z <= Dt*fabs(pz/sqrt(1. + pr*pr + pp*pp + pz * pz));
		
		bool CondInj =  InFocus && Bound1 && Bound2 && CondCurr ;
		
		if( CondInj ) {

			++count;
			
			if( count % world.MPIconf.SizeDepth == world.MPIconf.RankDepth ){


				particles(kmax).r = r ;
				particles(kmax).pz = pz;
					
				if(LeaftInj){
					  particles(kmax).z = z + Dz *  world.region.dampCells_d1[0];
					  particles(kmax).pp = pp;
					  particles(kmax).pr = pr;
				}
				else if ( RightInj){
					  particles(kmax).z = Dz * world.region.numCells_d1 - z - Dz *  world.region.dampCells_d1[1];
					  particles(kmax).pp = - pp;
					  particles(kmax).pr = - pr;
					}
					else{
					  std::cout << "Error Inject Particles type" << std::endl; 
					  exit(0); 
				}
				
				particles(kmax).density = std::min(1.,Dt*timestep/InjectSmoothTime)*dens * PI * Dr * Dz * width / (NumPartPerCell);
#if MASS_OWN == 1
		                   particles(kmax).M = particles.mass;
#endif	
				gama = sqrt(1. + pr*pr + pp*pp + pz*pz);
				energyInj += particles(kmax).density*(gama - 1.);
				++kmax;	
			}
		}
	}
			
	particles.particlesData.size_ = kmax;
	particles.injectionEnergy += energyInj;
}

*/