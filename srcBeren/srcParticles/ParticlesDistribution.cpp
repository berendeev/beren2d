#include "Particles.h"
#include "World.h"
#include "Shape.h"

inline double pow2(double a){
  return a*a;
}
inline double pow3(double a){
  return a*a*a;
}
inline double pow4(double a){
  return a*a*a*a;
}

inline double smooth_sin(double dist){
  if (fabs(dist) > 1.) return 0;
  else
  return 0.5*(1.+cos(dist*PI));
}

double set_density_neutron(double2 r){
	return exp( -pow4( ( r.x() ) / (1.2 * Dx * PlasmaCellsX_glob / 2.) ));
//	return exp( -pow4( (x(0)-Dz*PlasmaCellsZ_glob/2)/(1.2*Dz*PlasmaCellsZ_glob/2) ));
}


void ParticlesArray::set_space_distribution(){
	
	long startX = DampCellsX_glob[0];
	long endX = NumCellsX_glob - DampCellsX_glob[1];
	long startY = 0.5*(NumCellsY_glob - long(width/Dy));
	long endY = 0.5*(NumCellsY_glob + long(width/Dy));

	if( initDist == "StrictUniform"){
		set_strict_uniform(startX, endX, startY,endY);
		Particle particle;
		double x = 27.5;

		particle.coord.x() = x - _world.region.origin;
		particle.coord.y() = 25.;
		particle.pulse.x() = -2;
		particle.pulse.y() = 0;
		particle.pulse.z() = 0;

		//if(_world.region.in_region(x) ) 
		//		add_particle_scatter(particle);
	}
	
	if( initDist == "UniformCosX_dn_k"){
//		set_strict_cosX(startX, endX, startY,endY, distParams[0], distParams[1]);
	}
		#ifdef PARTICLE_MASS
			if (option.smoothMass == 1){
				set_smooth_mass();
			}
			else{
					for (auto k = 0; k < size(); ++k){
				//		particlesData(k).mass = _mass;
					}
			}
		#endif
		
	//	if (charge == 0) 
	//		set_distribution_density(world.region,set_density_neutron);
}
void  ParticlesArray::set_pulse_distribution(){
		auto sigma = temperature;
    for ( long i = 0; i < particlesData.size().x(); ++i){
    for ( long j = 0; j < particlesData.size().y(); ++j){

    for ( ulong k = 0; k < particlesData(i,j).size(); ++k){     
    	particlesData(i,j)(k).pulse.x() = Gauss(sigma);
			particlesData(i,j)(k).pulse.y() = Gauss(sigma);
			particlesData(i,j)(k).pulse.z() = Gauss(sigma);
		} 
	}
}

}




void ParticlesArray::set_strict_uniform(long startX, long endX, long startY, long endY){
    const double deltaX = Dx / NumPartPerLine;
    const double deltaY = Dy / NumPartPerLine;
    Particle particle;
    double x, y;
    
    x = -0.5 * deltaX;

    for( auto j = 0; j < NumPartPerLine * (endX - startX); ++j){
		x += deltaX;
		y = -0.5 * deltaY + Dy * startY;
		if( ! _world.region.in_region(x + Dx * startX) ) continue;

		    for ( auto i = 0; i < NumPartPerLine * (endY-startY); ++i){
				y += deltaY;
				particle.coord.x() = x + Dx * startX - _world.region.origin;
				particle.coord.y() = y;
				#ifdef PARTICLE_MPW
					particle.mpw = density * Dx * Dy / (NumPartPerLine*NumPartPerLine);
				#endif
				//std::cout<< particle.r.x() << " " << particle.r.y() <<  " " << particlesData.size() <<"\n";

				add_particle_scatter(particle);
		
			}

    }
}

/*
void ParticlesArray::set_strict_cosX(long startX, long endX, long startY, long endY, double delta_n0, double k0){
    const long Nd = 10000000;
    const double hd = Dx * (endX - startX) / Nd;
    const double deltaY = Dy / NumPartPerLine;
    double x, y;	
    double SS = 0.;
	Particle particle;

    for (auto i = 0; i < Nd; ++i){
		SS += ( (1.0 + delta_n0 * cos( i * k0 * hd)) * hd);
    }

    auto NpX = NumPartPerLine  * (endX - startX);
	
    auto mySS = SS / NpX;
      
    x = -0.5 * mySS;

    for( auto j = 0; j < NumPartPerLine * (endX - startX); ++j){
		x += (mySS / (1.0 + delta_n0 * cos( k0 * x) ) );
		y = -0.5 * deltaY + Dy * startY;
		if( !_world.region.in_region(x + Dx * startX) ) continue;

		for ( auto i = 0; i < NumPartPerLine * (endY-startY); ++i){
				y += deltaY;

			    particle.coord.x() = x + Dx * startX - _world.region.origin;
			    particle.coord.y() = y;
				#ifdef PARTICLE_MPW
				    particle.mpw = density * Dx * Dy / (NumPartPerLine*NumPartPerLine);
				#endif
					add_particle_scatter(particle);

		}

    }
		
}


void ParticlesArray::set_smooth_mass(){
	#ifdef PARTICLE_MASS
	    double x;
	    double2 r;

		for (auto k = 0; k < size(); ++k){
			r =  _world.region.get_coord_glob(particlesData(k).coord);  
			if (r.x() < 0.5 * Dx* NumCellsX_glob) 
				x = r.x() - Dx * DampCellsX_glob[0];
			else
				x = Dx * NumCellsX_glob - Dx * DampCellsX_glob[1] - r.x();
			particlesData(k).mass = (option.smoothMassMax * smooth_sin( x  / option.smoothMassSize ) + 1. ) * _mass;
		}
	#else
		std::cout << "Class Particle has not a mass\n";
	#endif
}

void ParticlesArray::set_distribution_density(std::function<double(double2 )> set_density){
	#ifdef PARTICLE_MPW
    double x, y, f;
    double2 r;

	for (auto k = 0; k < size(); ++k){
		x = particlesData(k).coord.x() - Dx*DampCellsX_glob[0];
		y = particlesData(k).coord.y();
		r =  world.region.getGlobCoord(double2(x,y));  
		f = set_density(r);
		particlesData(k).mpw *= f;
	}
	#endif
		
}*/