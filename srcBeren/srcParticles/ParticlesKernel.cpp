#include "Particles.h"
#include "Shape.h"

inline double layer_resumption_left( const Region& region){
	return Dx * (region.dampCells_d1[0] + 1);
}
inline double layer_resumption_right( const Region& region){
	return Dx * (region.numCells_d1 - region.dampCells_d1[1] - 1);
}

inline int iround(double x){
    if ( x < 0 ) x -= 0.5;
        else x += 0.5;
    return (int) x;
}

void CopyToBuf(const Particle& particleSource, Array<Particle>& ParticlesBuf, const Region& domain){
  Particle particle = particleSource;
  particle.set_global(domain);
  ParticlesBuf.push_back(particle);
//  if(particle.pulse.x()>0)
//  std::cout<<particle<<"\n";
}

void addFromBuf(Array2D< Array<Particle> >& particlesData,Array<Particle>& ParticlesBuf, const Region& domain){
  	Particle particle;
 	while( ParticlesBuf.size() > 0){
  	   	particle = ParticlesBuf.back();
  	   	ParticlesBuf.pop_back();
		particle.set_local(domain);
		auto i = long(particle.coord.x() / Dx);
    auto j = long(particle.coord.y() / Dy);
		particlesData(i,j).push_back(particle);
	//	  if(particle.pulse.x()>0)
	//	std::cout<<particle<<"\n";
  }
}


void MPIExchangeParticles(Array<Particle>& ParticlesBufLeft, Array<Particle>& ParticlesBufRight, const MPI_Topology& MPIconf){
  const long maxSize = ParticlesBufLeft.capacity();
  static Array<Particle> RecvBufLeft;
  static Array<Particle> RecvBufRight;
  long guestLeft, guestRight, sizeLeft,sizeRight;
  int tag = 2;
  long k;
  long sizeParticleStruct = iround(sizeof(Particle) / sizeof(double));
  MPI_Status status;
  
	auto right = MPIconf.next_line();
  
  	auto left = MPIconf.prev_line();
	static int ttim =0;
	if (ttim==0) {
	RecvBufLeft.reserve(maxSize);
	RecvBufRight.reserve(maxSize);
	ttim =1;
	}
	sizeLeft = ParticlesBufLeft.size();
	sizeRight = ParticlesBufRight.size();

  MPI_Sendrecv(&sizeLeft, 1, MPI_LONG, left, tag, 
                           &guestRight, 1, MPI_LONG, right, tag, 
                           MPIconf.comm_line(), &status);
  MPI_Sendrecv(&sizeRight, 1, MPI_LONG, right, tag, 
                           &guestLeft, 1, MPI_LONG, left, tag, 
                           MPIconf.comm_line(), &status);


  //MPI_Barrier(MPIconf.CommLine);
  MPI_Sendrecv(&ParticlesBufLeft(0), (sizeLeft + 1) *  sizeParticleStruct, MPI_DOUBLE_PRECISION, left, tag, 
                           &RecvBufRight(0), (guestRight + 1) *  sizeParticleStruct, MPI_DOUBLE_PRECISION, right, tag, 
                           MPIconf.comm_line(), &status);
  MPI_Sendrecv(&ParticlesBufRight(0), (sizeRight + 1) *  sizeParticleStruct, MPI_DOUBLE_PRECISION, right, tag, 
                           &RecvBufLeft(0), (guestLeft + 1) *  sizeParticleStruct, MPI_DOUBLE_PRECISION, left, tag, 
                           MPIconf.comm_line(), &status);
   // MPI_Barrier(MPIconf.CommLine);
	ParticlesBufLeft.clear();
	ParticlesBufRight.clear();
  	
  	for(k = 0; k < guestLeft; ++k){
  		ParticlesBufLeft.push_back(RecvBufLeft(k) );
  	}
   
  	for(k = 0; k < guestRight; ++k){
      	ParticlesBufRight.push_back(RecvBufRight(k) ); 
  	}

}

/*
void current_in_cell(double2 r, double2 r1, long indx, long indy, double vz,double koeff, Array2D<double3>& fieldJ){
	double dx = r1.x() - r.x();
	double dy = r1.y() - r.y();
	double sx =  (0.5*(r1.x() + r.x() ) - (indx-1)*Dx ) / Dx;
	double sy =  (0.5*(r1.y() + r.y() ) - (indy-1)*Dy ) / Dy;

	fieldJ(indx,indy).x()     += koeff * dx *  (1-sy ); 
	fieldJ(indx,indy+1).x()   += koeff * dx * (sy ); 

	fieldJ(indx,indy ).y()     += koeff * dy *  (1-sx ) ; 
	fieldJ(indx+1,indy).y()   += koeff * dy * (  sx  ); 

	fieldJ(indx,indy).z()     += koeff * vz*Dt * (  (1-sx ) * (1-sy ) + dx*dy/(12*Dx*Dy) ); 
	fieldJ(indx,indy+1).z()   += koeff * vz*Dt * (  (1-sx ) * sy      - dx*dy/(12*Dx*Dy) ); 
	fieldJ(indx+1,indy).z()   += koeff * vz*Dt * (  sx      * (1-sy ) - dx*dy/(12*Dx*Dy) ); 
	fieldJ(indx+1,indy+1).z() += koeff * vz*Dt * (  sx      * sy      + dx*dy/(12*Dx*Dy) ); 
}

void push_pic(double2& POS, double3& PULS, long q, double mass, double mpw, const Array2D<double3>& fieldE, \
		   const Array2D<double3>& fieldB, Array2D<double3>& fieldJ, double3 E = double3(0.,0.,0.) ){
	long indx, indy,indx1, indy1,indx_n, indy_n;
	double gama;
	double3 US,U1,U2,T,C;
	double xn, yn;
	double xx,yy,vs,vs1;
	double xx_n, yy_n;	
	double a,b;
	double sx0,sy0,sdx0,sdy0;
	double sx1,sy1,sdx1,sdy1;
	double xp,yp,s;
//	alignas(64) double3 E;
	alignas(64) double3 B;

	double2 POS1;
	const double rdx = 1. / Dx;
	const double rdy = 1. / Dy;
	const double dtp = 0.5 * Dt;
	const double koeff = q / (Dt*Dx*Dy) * mpw;
	int dir; 
	xx = POS.x() * rdx;
	yy = POS.y() * rdy;

	indx = long(xx+1.);
	indy = long(yy+1.);

	indx1 = long(xx+0.5);
	indy1 = long(yy+0.5);

    sx1 = (xx - indx+1);
    sy1 = (yy - indy+1);

    sdx1 = (xx - indx1+0.5);
    sdy1 = (yy - indy1+0.5);


    sx0 = 1. - sx1;
    sy0 = 1. - sy1;
    sdx0 = 1. - sdx1;
    sdy0 = 1. - sdy1;

	E.x() += sdx0 * ( sy0 *  fieldE(indx1,indy).x() 
		          + sy1 * fieldE(indx1,indy+1).x() ) 
			+ sdx1 * ( sy0 * fieldE(indx1+1,indy).x() 
		          + sy1 *  fieldE(indx1+1,indy+1).x()  );

	E.y() += sx0 * ( sdy0 *  fieldE(indx,indy1).y()  
		          + sdy1 * fieldE(indx,indy1+1).y() )  
			+ sx1 * ( sdy0 *  fieldE(indx+1,indy1).y()  
		          + sdy1 *  fieldE(indx+1,indy1+1).y()  );

	E.z() += sx0 * ( sy0 *  fieldE(indx,indy).z()  
		          + sy1 * fieldE(indx,indy+1).z() ) 
			+ sx1 * ( sy0 * fieldE(indx+1,indy).z()  
		          + sy1 *  fieldE(indx+1,indy+1).z()  );

	B.x() += sx0 * ( sdy0 * fieldB(indx,indy1).x() 
		           + sdy1 * fieldB(indx,indy1+1).x() ) 
		   + sx1 * ( sdy0 * fieldB(indx+1,indy1).x() 
		           + sdy1 * fieldB(indx+1,indy1+1).x() );

	B.y() += sdx0 * ( sy0 * fieldB(indx1,indy).y()
		          + sy1 * fieldB(indx1,indy+1).y() ) 
			+ sdx1 * ( sy0 * fieldB(indx1+1,indy).y() 
		          + sy1 * fieldB(indx1+1,indy+1).y() );

	B.z() += sdx0 * ( sdy0 * fieldB(indx1,indy1).z()  
		          + sdy1 *fieldB(indx1,indy1+1).z() ) 
			+ sdx1 * ( sdy0 * fieldB(indx1+1,indy1).z()  
		          + sdy1 * fieldB(indx1+1,indy1+1).z() );

	U1 = PULS + q * dtp * E;
	a = q * dtp / sqrt(1. + dot(U1,U1) );
	T = a * B;
	b = 2. / (1. + dot(T,T) );
	C = b * T;
		
	US = U1 + cross(U1,T);
		
	U2 = U1 + cross(US,C);
		
	PULS = U2 + q * dtp * E;
		
	gama = 1. / sqrt(mass * mass + dot(PULS,PULS) );
			
	xn = POS.x() + Dt * PULS.x() * gama;
	yn = POS.y() + Dt * PULS.y() * gama;

	POS1 = double2(xn,yn);
	xx_n = POS1.x() * rdx;
	yy_n = POS1.y() * rdy;

	indx_n = long(xx_n+1.);
	indy_n = long(yy_n+1.);

	bool mx = indx - indx_n;
	bool my = indy - indy_n;

	dir = 4*mx + 2*my;
	if (!dir){
		current_in_cell(POS, POS1, indx, indy, PULS.z() * gama,koeff, fieldJ);
	}
	else{
		switch (dir){

			case 2: // y cell intersection
				yp = Dy*(0.5*(indy_n + indy) - 0.5);
				s = (yp - POS.y())/(POS1.y() - POS.y() );
				xp = POS.x() + s*(POS1.x()-POS.x() );
				vs = PULS.z() * gama * s;
				vs1 = PULS.z() * gama - vs;
				break;
			case 4: // x cell intersection 
				xp = Dx*(0.5*(indx_n + indx) - 0.5);
				s = (xp - POS.x())/(POS1.x() - POS.x() );
				yp = POS.y() + s*(POS1.y()-POS.y() );
				vs = PULS.z() * gama * s;
				vs1 = PULS.z() * gama - vs;
				break;	
			case 6: //x and y intersection
				xp = Dx*(0.5*(indx_n + indx) - 0.5);
				yp = Dy*(0.5*(indy_n + indy) - 0.5);
				s = ( (POS1.x()-POS.x()) * (xp-POS.x()) + (POS1.y()-POS.y()) * (yp-POS.y()) )
				/ ( (POS1.x()-POS.x()) * (POS1.x()-POS.x()) + (POS1.y()-POS.y()) * (POS1.y()-POS.y()) );
				vs = PULS.z() * gama * s;
				vs1 = PULS.z() * gama - vs;
				break;
			default: 
				xp = -1;
				yp = -1;
				vs = -1;
				vs1 = -1;
				assert( false && "Error in calculation cell intersection\n");
		}
		current_in_cell(POS, double2(xp,yp), indx, indy,vs,koeff, fieldJ);
		current_in_cell(double2(xp,yp), POS1, indx_n, indy_n, vs1,koeff, fieldJ);
	}

	POS = POS1;
}

*/  	

void push(double2& POS, double3& PULS, long q, double mass,double mpw,long& xk,long& yk,\
			 //const double3(&fieldE)[2*SHAPE_SIZE][2*SHAPE_SIZE],\
		   const double3(&fieldB)[2*SHAPE_SIZE][2*SHAPE_SIZE], double3 (&fieldJ)[2*SHAPE_SIZE][2*SHAPE_SIZE], 
		   Interp& interp, double3 E = double3(0.,0.,0.) ){
  	constexpr auto SMAX = 2*SHAPE_SIZE;
	long n, m, indx, indy;
	double xx, yy, arg;
	double snm1,snm2,snm3,snm4,gama;
	double3 US,U1,U2,T,C;
	double xn, yn;
	double a,b,tokz;
	alignas(64) double sx[SMAX];
	alignas(64) double sy[SMAX];
	alignas(64) double sdx[SMAX];
	alignas(64) double sdy[SMAX];
	alignas(64) double sx_n[SMAX];
	alignas(64) double sy_n[SMAX];
	alignas(64) double jx[SMAX][SMAX];
	alignas(64) double jy[SMAX][SMAX];
	alignas(64) double jz[SMAX][SMAX];
	//alignas(64) double3 E;
	alignas(64) double3 B;

	const double rdx = 1. / Dx;
	const double rdy = 1. / Dy;
	const double dtp = 0.5 * Dt;
	const double conx = 0.5 * Dx / Dt * mpw;
	const double cony = 0.5 * Dy / Dt * mpw;
			
	xx = POS.x() * rdx;
	yy = POS.y() * rdy;

	//xk = long(xx);
	//yk = long(yy);
	
	for(n = 0; n < SMAX; ++n){
		arg = -xx + double(xk - CELLS_SHIFT + n);
		sx[n] = Shape(arg)/ Dx;
		sdx[n] = Shape(arg + 0.5)/ Dx;
		arg = -yy + double(yk - CELLS_SHIFT + n);
		sy[n] = Shape(arg)/ Dy;
		sdy[n] = Shape(arg + 0.5)/ Dy;
	}
		
	for(n = 0; n < SMAX; ++n){
			//indx = xk + n;

		for(m = 0; m < SMAX; ++m){
			jx[n][m] = 0.;
			jy[n][m] = 0.;
			jz[n][m] = 0.;
			
			snm1 = sdx[n] * sy[m];
			snm4 = sx[n] * sy[m];
			snm2 = sx[n] * sdy[m];
			snm3 = sdx[n] * sdy[m];

			E.x() += Dx * Dy * snm1 * interp.Ex[n][m];
			E.y() += Dx * Dy * snm2 * interp.Ey[n][m];
			E.z() += Dx * Dy * snm4 * interp.Ez[n][m];
			B.x() += Dx * Dy * snm2 * interp.Bx[n][m];
			B.y() += Dx * Dy * snm1 * interp.By[n][m];
			B.z() += Dx * Dy * snm3 * interp.Bz[n][m];
		}
	}

	U1 = PULS + q * dtp * E;
	a = q * dtp / sqrt(1. + dot(U1,U1) );
	T = a * B;
	b = 2. / (1. + dot(T,T) );
	C = b * T;
		
	US = U1 + cross(U1,T);
		
	U2 = U1 + cross(US,C);
		
	PULS = U2 + q * dtp * E;
		
	gama = 1. / sqrt(mass * mass + dot(PULS,PULS) );
			
	xn = POS.x() + Dt * PULS.x() * gama;
	yn = POS.y() + Dt * PULS.y() * gama;
	POS.x() = xn;
	POS.y() = yn;

	for(n = 0; n < SMAX; ++n){
		arg = -xn * rdx + double(xk - CELLS_SHIFT + n);
		sx_n[n] = Shape(arg)/ Dx;
		arg = -yn * rdy + double(yk - CELLS_SHIFT + n);
		sy_n[n] = Shape(arg)/ Dy;
	}

	tokz = PULS.z() * gama / 6. * mpw;

	for(n = 0; n < SMAX; ++n){
		for(m = 0; m < SMAX; ++m){
		  
			if(n == 0) jx[n][m] = -q * conx * (sx_n[n] - sx[n]) * (sy_n[m] + sy[m]);
			if(n > 0 && n < SMAX-1) jx[n][m] = jx[n-1][m] - q * conx * (sx_n[n] - sx[n]) * (sy_n[m] + sy[m]);
			if(m == 0) jy[n][m] = -q * cony * (sx_n[n] + sx[n]) * (sy_n[m] - sy[m]);
			if(m > 0 && m < SMAX-1) jy[n][m] = jy[n][m-1] - q * cony * (sx_n[n] + sx[n]) * (sy_n[m] - sy[m]);
			if(n < SMAX-1 && m < SMAX-1) jz[n][m] = q * tokz * (sy_n[m] * (2*sx_n[n] + sx[n]) + sy[m] * (2 * sx[n] + sx_n[n]));

			interp.Jx[n][m] += jx[n][m];
			interp.Jy[n][m] += jy[n][m];
			interp.Jz[n][m] += jz[n][m];

		}
	}
	xk = long((POS.x()+Dx) * rdx) -1 ;
	yk = long((POS.y()+Dy) * rdy) -1;
		
}

void ParticlesArray::bound_resumption(Particle& particle, double2& r, double3& p){

	if( option.boundResumption ==0 ) return;
			
			long i,j;
			double layer;
		  layer = layer_resumption_left(_world.region);
		  bool addFromLeft = _world.region.boundType_d1[0] == OPEN 
		  				&& particle.coord.x() <= layer && r.x() > layer;
		  layer = layer_resumption_right(_world.region);
		  bool addFromRight = _world.region.boundType_d1[1] == OPEN 
		  				&& particle.coord.x() >= layer && r.x() < layer;
		  
		  if (addFromLeft || addFromRight){
		    
			particle.coord.x() = (addFromLeft ? r.x()-Dx : r.x()+Dx);
			particle.pulse = double3(p.x(),Gauss(temperature), Gauss(temperature) );
			i = long(particle.coord.x() /Dx);
			j = long(particle.coord.y()/Dy );
			particlesData(i,j).push_back(particle);		
			//std::cout<< i << " " << j << " "<< particle << "\n";
    
			}
	
}

void ParticlesArray::move(Mesh& mesh,long timestep){
	  	constexpr auto SMAX = 2*SHAPE_SIZE;

	double2 r, r_glob, r_old;
	double3 p;
	Particle particle;
	bool in_area;
	bool lostXLeft, lostXRight, lostY;
	double layer;
	const long ParticlesBufSize = 2*MaxSizeOfParts / (_world.region.numCells_d1);
	static Array<Particle> ParticlesBufLeft;
	static Array<Particle> ParticlesBufRight;
	static int ttim =0;
	if (ttim==0) {
	ParticlesBufLeft.reserve(10000);
	ParticlesBufRight.reserve(1000);
	ttim =1;
	}
	//Array2D<double3> E(SMAX,SMAX);
	//Array2D<double3> B(SMAX,SMAX);
	//Array2D<double3> J(SMAX,SMAX);
//	alignas(64) double3 E[SMAX][SMAX];
//	alignas(64) double3 B[SMAX][SMAX];
//	alignas(64) double3 J[SMAX][SMAX];
	Interp interp;
	long n,m,indx,indy;
	if (charge == 0) return;

	//long kmax = size();

  for ( long i = 0; i < particlesData.size().x()-SMAX+1; ++i){
		for ( long j = CELLS_SHIFT; j < particlesData.size().y()-SMAX+1+ CELLS_SHIFT; ++j){
		
		for(n = 0; n < SMAX; ++n){
			//indx = i + n;

		for(m = 0; m < SMAX; ++m){
			
		//	indy = j - CELLS_SHIFT + m;
	
			interp.Ex[n][m] = mesh.fieldE(i + n,j - CELLS_SHIFT + m).x() ;
			interp.Ey[n][m] = mesh.fieldE(i + n,j - CELLS_SHIFT + m).y() ;
			interp.Ez[n][m] = mesh.fieldE(i + n,j - CELLS_SHIFT + m).z() ;
			interp.Bx[n][m] =  mesh.fieldB(i + n,j - CELLS_SHIFT + m).x() ;
			interp.By[n][m] =  mesh.fieldB(i + n,j - CELLS_SHIFT + m).y() ;
			interp.Bz[n][m] =  mesh.fieldB(i + n,j - CELLS_SHIFT + m).z() ;
			interp.Jx[n][m] = 0. ;
			interp.Jy[n][m] = 0. ;
			interp.Jz[n][m] = 0. ;

		}
	}


			long k = 0;

    	while( k < countInCell(i,j) ){  
    		//	if (k==0.) std::cout<< i << " " << j << " " << countInCell(i,j)  << "\n";	

    		particle = particlesData(i,j)(k);  		
    	//	std::cout<< i << " " << j << " " << particle<<"\n";

    		r = particle.coord;
				p = particle.pulse;
				double3 ELas;	
		
				for (const auto& las : mesh.lasers){
					r_glob = _world.region.get_coord_glob(r);
					ELas += las.force(r_glob,timestep);
		//	std::cout<< las.force(double2(7.,28),20) << "\n";
		//	std::cout<< las.force(double2(67,28),20) << "\n";
				}
				//if (ELas.x()!=0.) std::cout<< ELas << "\n";	
		//exit(-1);
				#if SHAPE == 1
	//		push_pic(r,p, charge, mass(i,j,k), mpw(i,j,k), mesh.fieldE, mesh.fieldB, mesh.fieldJ, ELas);
				#else
				
					long i1 = i;
					long j1 = j;
					push(r,p, charge, mass(i,j,k), mpw(i,j,k), i1,j1,interp, ELas);
				#endif

		/*if( option.boundResumption == 1){

		  layer = layer_resumption_left(_world.region);
		  bool addFromLeft = _world.region.boundType_d1[0] == OPEN 
		  				&& particlesData(k).coord.x() <= layer && r.x() > layer;
		  layer = layer_resumption_right(_world.region);
		  bool addFromRight = _world.region.boundType_d1[1] == OPEN 
		  				&& particlesData(k).coord.x() >= layer && r.x() < layer;
		  
		  if (addFromLeft || addFromRight){
		    
		    particle = particlesData(k);
			particle.coord.x() = (addFromLeft ? r.x()-Dx : r.x()+Dx);
			particle.coord.y() = r.y();
			particle.pulse = double3(p.x(),Gauss(temperature), Gauss(temperature) );
			particlesData.push_back(particle);		    
			}
		}	*/	
				if(i == i1 && j == j1){
					particlesData(i,j)(k).coord = r;
					particlesData(i,j)(k).pulse = p;
					k++;
				}
				else {

					delete_particle_runtime(i,j,k);
					bound_resumption(particle,r,p);
					
					particle.coord = r ;
					particle.pulse = p;

					lostXLeft = (r.x() < Dx * _world.region.dampCells_d1[0]);
			 
					lostXRight = (r.x() >= Dx*(_world.region.numCells_d1 - _world.region.dampCells_d1[1]));
			
					lostY = (r.y() <= 10.*Dy || r.y() >= Dy*_world.region.numCells_d2 - 10*Dy);
			
					in_area = ! (lostXLeft || lostXRight || lostY);
			
					if( in_area ) 
						particlesData(i1,j1).push_back(particle);
					else{
			  
				  	if(lostXLeft && _world.region.boundType_d1[0] == NEIGHBOUR){
				    	CopyToBuf(particle,ParticlesBufLeft, _world.region);
				  	}
				  	if(lostXRight && _world.region.boundType_d1[1] == NEIGHBOUR){
				    	CopyToBuf(particle,ParticlesBufRight, _world.region);
				  	}
			  
					}
	
				}
		//k++;
			}
				for(n = 0; n < SMAX; ++n){
						//indx = i + n;

					for(m = 0; m < SMAX; ++m){
			
						//indy = j - CELLS_SHIFT + m;
	
						 mesh.fieldJ(i + n,j - CELLS_SHIFT + m).x() += interp.Jx[n][m];
						 mesh.fieldJ(i + n,j - CELLS_SHIFT + m).y() += interp.Jy[n][m];
						 mesh.fieldJ(i + n,j - CELLS_SHIFT + m).z() += interp.Jz[n][m];

					}
				}
		}
	}

	MPIExchangeParticles(ParticlesBufLeft, ParticlesBufRight,_world.MPIconf);

	addFromBuf(particlesData, ParticlesBufLeft, _world.region);
	addFromBuf(particlesData, ParticlesBufRight, _world.region);
	update_count_in_cell();


}

void ParticlesArray::move_virt(Mesh& mesh,long timestep){
	  	constexpr auto SMAX = 2*SHAPE_SIZE;

	double2 r, r_glob;
	double3 p;
	Particle particle;
	long xL, xR;
	double rx;
	long virt;
	Interp interp;
	long n,m,indx,indy;
	long i2;
	if (charge == 0) return;

  for ( long i = 0; i < particlesData.size().x()-SMAX+1; ++i){
		xL = _world.region.dampCells_d1[0] ;
		xR = _world.region.numCells_d1 - _world.region.dampCells_d1[1]-1;
		virt = 0;
		if( i>= xL && i  < xL + 2 &&  _world.region.boundType_d1[0] == OPEN){
			virt = 1;
			i2 = -2;
			rx = -2*Dx;
		}
		if(i <= xR && i > xR - 2  &&  _world.region.boundType_d1[1] == OPEN){
			rx = 2*Dx;
			i2 = 2;
			virt = 1;
		}
		
		if(virt == 0) continue;
		for ( long j = CELLS_SHIFT; j < particlesData.size().y()-SMAX+1+ CELLS_SHIFT; ++j){
		
		for(n = 0; n < SMAX; ++n){
		for(m = 0; m < SMAX; ++m){	
			interp.Ex[n][m] = mesh.fieldE(i + i2+ n,j - CELLS_SHIFT + m).x() ;
			interp.Ey[n][m] = mesh.fieldE(i + i2+n,j - CELLS_SHIFT + m).y() ;
			interp.Ez[n][m] = mesh.fieldE(i + i2+n,j - CELLS_SHIFT + m).z() ;
			interp.Bx[n][m] =  mesh.fieldB(i + i2+n,j - CELLS_SHIFT + m).x() ;
			interp.By[n][m] =  mesh.fieldB(i + i2+n,j - CELLS_SHIFT + m).y() ;
			interp.Bz[n][m] =  mesh.fieldB(i +i2+ n,j - CELLS_SHIFT + m).z() ;
			interp.Jx[n][m] = 0. ;
			interp.Jy[n][m] = 0. ;
			interp.Jz[n][m] = 0. ;
		}
	}
			long k = 0;

    	while( k < countInCell(i,j) ){  
    		particle = particlesData(i,j)(k); 
    		//if(j == particlesData.size().y()/2 && k ==0) 		
				//std::cout<< i << " " << j << " "<< particle << "\n";

    		r = particle.coord +double2(rx,0.0);
				p = particle.pulse;
				double3 ELas;	
		
				for (const auto& las : mesh.lasers){
					r_glob = _world.region.get_coord_glob(r);
				//	ELas += las.force(r_glob,timestep);
				}

				#if SHAPE == 1
	//		push_pic(r,p, charge, mass(i,j,k), mpw(i,j,k), mesh.fieldE, mesh.fieldB, mesh.fieldJ, ELas);
				#else
				
					long i1 = i+ i2;
					long j1 = j;
					push(r,p, charge, mass(i,j,k), mpw(i,j,k), i1,j1,interp, ELas);
				#endif
			k++;
			}
				for(n = 0; n < SMAX; ++n){
					for(m = 0; m < SMAX; ++m){	
						 mesh.fieldJ(i + i2+n,j - CELLS_SHIFT + m).x() += interp.Jx[n][m];
						 mesh.fieldJ(i + i2+n,j - CELLS_SHIFT + m).y() += interp.Jy[n][m];
						 mesh.fieldJ(i + i2+n,j - CELLS_SHIFT + m).z() += interp.Jz[n][m];
					}
				}
		}
	}
	
}

/*
void ParticlesArray::move_virt(Mesh& mesh,long timestep){
	long virt;
	double x, y, xL,xR;
	double2 r;
	double3 p;
	if(charge == 0) return;
	if ( _world.region.boundType_d1[0] != OPEN && _world.region.boundType_d1[1] != OPEN) return;
	  
	long k=0;
	

	while (k < particlesData.size() ) {
		virt = 0;
		x = particlesData(k).coord.x();
		xL = Dx*(_world.region.dampCells_d1[0]) ;
		xR = Dx*(_world.region.numCells_d1 - _world.region.dampCells_d1[1]);

		if(x < xL + 2.*Dx &&  _world.region.boundType_d1[0] == OPEN){
			x = x - 2*Dx;
			//z = 2*zL-z;
			virt = 1;
		}
		if(x > xR - 2.*Dx  &&  _world.region.boundType_d1[1] == OPEN){
			x = x + 2*Dx;
			//z = 2*zR -z;
			virt = 1;
		}
		
		if(virt != 0){
		
			y = particlesData(k).coord.y();
			p = particlesData(k).pulse;

			r = double2(x,y);	
			#if SHAPE == 1
				push_pic(r,p, charge, mass(k), mpw(k), mesh.fieldE, mesh.fieldB, mesh.fieldJ);
			#else
				push(r,p, charge, mass(k), mpw(k), mesh.fieldE, mesh.fieldB, mesh.fieldJ);
			#endif	
		}
		
		k++;
	}
}
*/