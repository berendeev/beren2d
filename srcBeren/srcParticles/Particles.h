#ifndef PARTICLES_H_
#define PARTICLES_H_
#include "World.h"
#include "Vec.h"
#include "Mesh.h"
#include <functional>
#include <assert.h>

struct ParticleSimple{
	double2 coord;
    double3 pulse;
	friend std::ostream& operator<<(std::ostream& out, const ParticleSimple &particle);
    
    void set_global(const Region& domain){
        coord.x() += domain.origin;
    }
    void set_local(const Region& domain){
        coord.x() -= domain.origin;      
    }
};

struct ParticleMPW : ParticleSimple{
    double mpw;
    friend std::ostream& operator<<(std::ostream& out, const ParticleMPW &particle);

};
struct ParticleMass : ParticleSimple{
	double mass;
  	friend std::ostream& operator<<(std::ostream& out, const ParticleMass &particle);
};

struct ParticleMassMPW : ParticleSimple{
    double mass,mpw;
    friend std::ostream& operator<<(std::ostream& out, const ParticleMassMPW &particle);
};

#ifdef PARTICLE_MASS
    #ifdef PARTICLE_MPW
        typedef ParticleMassMPW Particle;
    #else
        typedef ParticleMass Particle;
    #endif
#else
    #ifdef PARTICLE_MPW
        typedef ParticleMPW Particle;
    #else
        typedef ParticleSimple Particle;
    #endif
#endif


struct ParticlesOption{
    long boundResumption;
    long sourceType;
    long smoothMass;
    double smoothMassSize;
    double smoothMassMax; 
};


struct Interp{
    alignas(64) double Ex[2*SHAPE_SIZE][2*SHAPE_SIZE];
    alignas(64) double Ey[2*SHAPE_SIZE][2*SHAPE_SIZE];
    alignas(64) double Ez[2*SHAPE_SIZE][2*SHAPE_SIZE];
    alignas(64) double Bx[2*SHAPE_SIZE][2*SHAPE_SIZE];
    alignas(64) double By[2*SHAPE_SIZE][2*SHAPE_SIZE];
    alignas(64) double Bz[2*SHAPE_SIZE][2*SHAPE_SIZE];
    alignas(64) double Jx[2*SHAPE_SIZE][2*SHAPE_SIZE];
    alignas(64) double Jy[2*SHAPE_SIZE][2*SHAPE_SIZE];
    alignas(64) double Jz[2*SHAPE_SIZE][2*SHAPE_SIZE];
    alignas(64) double3 J[2*SHAPE_SIZE][2*SHAPE_SIZE];


};

class ParticlesArray2{

public:
    Array<Particle> particlesData;

    Array2D<double> densityOnGrid;
    Array2D<double> phaseOnGrid;
    
    long charge;
    double density;
    double phasePXmin, phasePXmax;
    double pot_I;
    double pot_k;
    double kineticEnergy;
    double injectionEnergy;
    std::string name;
    double temperature;
    double velocity;
    double width;
    double focus;
    static long counter;
    ParticlesOption option;
    std::string initDist;
    std::vector<double> distParams;
    void add_particle_scatter(const Particle& particle){
        if(counter % _world.MPIconf.size_depth() == _world.MPIconf.rank_depth() )
            particlesData.push_back(particle);
        ++counter;
    }

    void set_distribution_density(std::function<double(double2 )> set_density);
    void set_smooth_mass();
    ParticlesArray2(const std::vector<std::string>& vecStringParams, World& world);
    void set_params_from_string(const std::string& line);
    void density_on_grid_update();
    void phase_on_grid_update();
    void set_distribution();
    void inject(long timestep);
    void update(Mesh& mesh,long timestep);
    double mass() const{
          return _mass;
    }
    double mass(long k) const{
        #ifdef PARTICLE_MASS
          return particlesData(k).mass;
        #else
          return _mass;
        #endif
    }
    double mpw(long k) const{
        #ifdef PARTICLE_MPW
          return particlesData(k).mpw;
        #else
          return _mpw;
        #endif
    }    
    //void read_recovery_particles(const MPI_Topology& MPIconf);
    Particle& operator() (long i) {
        return particlesData(i);
    }

    const Particle& operator() (long i) const{
        return particlesData(i);
    }
    long size() const{
        return particlesData.size();
    }
    double get_kinetic_energy() const;
    double get_kinetic_energy_half() const;
    double get_inject_energy() const{
        return injectionEnergy;
    }
    void move(Mesh& mesh,long timestep);
    void move_virt(Mesh& mesh,long timestep);

    void set_space_distribution();
    void set_pulse_distribution();
    void set_strict_uniform(long startX, long endX, long startY, long endY);
    void set_strict_cosX(long startX, long endX, long startY, long endY, double delta_n0, double k0);
    void source_uniform_from_bound(long timestep);
    void source_focused_gauss(long timestep);

    void read_from_recovery(const MPI_Topology &MPIconf);
    void write_recovery(const MPI_Topology &MPIconf) const;
protected:
    World &_world;
    double _mass;
    double _mpw; /*macroparticle weight*/
};


class ParticlesArray{

public:
    Array2D<Array<Particle> > particlesData;

    Array2D<double> densityOnGrid;
    Array2D<double> phaseOnGrid;
    Array2D<long> countInCell;
    
    long charge;
    double density;
    double phasePXmin, phasePXmax;
    double pot_I;
    double pot_k;
    double kineticEnergy;
    double injectionEnergy;
    std::string name;
    double temperature;
    double velocity;
    double width;
    double focus;
    static long counter;
    ParticlesOption option;
    std::string initDist;
    std::vector<double> distParams;
    void add_particle_scatter(const Particle& particle){
        auto i = long(particle.coord.x() / Dx);
        auto j = long(particle.coord.y() / Dy);
        if(counter % _world.MPIconf.size_depth() == _world.MPIconf.rank_depth() ){
            particlesData(i,j).push_back(particle);
        
        }
        ++counter;
    }
    void delete_particle_runtime(long i, long j, long k){
        countInCell(i,j)--;
        long old_count = countInCell(i,j);
        particlesData(i,j)(k) = particlesData(i,j)(old_count);
        if(old_count == particlesData(i,j).size()-1 ){
             particlesData(i,j).pop_back();
        }
        else{
            particlesData(i,j).del(old_count);
        }
    }
    void update_count_in_cell(){
        for (auto i = 0;i< countInCell.size().x();i++){
        for (auto j = 0; j< countInCell.size().y();j++){
            countInCell(i,j) = particlesData(i,j).size();
        }
        }
    }
    long size(){
        long s = 0;
        for (auto i = 0;i< countInCell.size().x();i++){
        for (auto j = 0; j< countInCell.size().y();j++){
            s+= particlesData(i,j).size();
        }
        }
        return s;
    }
    //void set_distribution_density(std::function<double(double2 )> set_density);
    //void set_smooth_mass();
    ParticlesArray(const std::vector<std::string>& vecStringParams, World& world);
    void set_params_from_string(const std::string& line);
    void density_on_grid_update();
    void phase_on_grid_update();
    void set_distribution();
    void inject(long timestep);
    void update(Mesh& mesh,long timestep);
    double mass() const{
          return _mass;
    }
    double mass(long i, long j,long k) const{
        #ifdef PARTICLE_MASS
          return particlesData(i,j)(k).mass;
        #else
          return _mass;
        #endif
    }
    double mpw(long i, long j,long k) const{
        #ifdef PARTICLE_MPW
          return particlesData(i,j)(k).mpw;
        #else
          return _mpw;
        #endif
    }    
    //void read_recovery_particles(const MPI_Topology& MPIconf);
   // Particle& operator() (long i) {
   //     return particlesData(i);
    //}

   // const Particle& operator() (long i) const{
    //    return particlesData(i);
   // }
    //long size() const{
    //    return particlesData.size();
    //}
    double get_kinetic_energy() const;
    double get_kinetic_energy_half() const;
    double get_inject_energy() const{
        return injectionEnergy;
    }
    void move(Mesh& mesh,long timestep);
    void move_virt(Mesh& mesh,long timestep);
    void bound_resumption(Particle& particle, double2& r, double3& p);

    void set_space_distribution();
    void set_pulse_distribution();
    void set_strict_uniform(long startX, long endX, long startY, long endY);
  //  void set_strict_cosX(long startX, long endX, long startY, long endY, double delta_n0, double k0);
    //void source_uniform_from_bound(long timestep);
    //void source_focused_gauss(long timestep);

    //void read_from_recovery(const MPI_Topology &MPIconf);
    //void write_recovery(const MPI_Topology &MPIconf) const;
protected:
    World &_world;
    double _mass;
    double _mpw; /*macroparticle weight*/
};



double PulseFromKev(double kev, double mass);

int get_num_of_type_particles(const std::vector<ParticlesArray> &Particles, const std::string& ParticlesType);
/// Ionization particles = electron(particles_e) +  ion (particles_i) 
void collision(const Mesh &mesh, const World& world ,std::vector<ParticlesArray> &Particles,long timestep);
double getFieldEInLaser(double z, double r, const Region& domain, long timestep);

#endif 
