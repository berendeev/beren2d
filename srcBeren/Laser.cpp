#include "Laser.h"
#include "const.h"

inline double pow2(double a){
  return a*a;
}
inline double pow3(double a){
  return a*a*a;
}
inline double pow4(double a){
  return a*a*a*a;
}

Laser::Laser(const std::vector<std::string>& vecStringParams){
    for (const auto& line: vecStringParams){
        set_params_from_string(line);
    }

}

// Устанавливаем значения основных параметров в переменной Params в соответствии с содержимым сроки
void Laser::set_params_from_string(const std::string& line){
    std::vector<std::string> strvec;

    strvec = split(line, ' ');

    if(strvec[0]=="tau"){
        tau = stod(strvec[1]);
    }

    if(strvec[0]=="w0"){
        w0 = stod(strvec[1]);
    }
    if(strvec[0]=="type"){
        type = strvec[1];
    }
    if(strvec[0]=="y0"){
        y0 = stod(strvec[1]);
    }
    if(strvec[0]=="vg"){
       vg =  stod(strvec[1]);
    }
    if(strvec[0]=="sigma0"){
       sigma0 =  stod(strvec[1]);
    }
    if(strvec[0]=="focus_x"){
        focus_d1 = stod(strvec[1]);
    }
    if(strvec[0]=="start_x"){
        start_d1 = stod(strvec[1]);
    }
    if(strvec[0]=="a0"){
       a0 = stod(strvec[1]);
    }
    if(strvec[0]=="delay"){
        delay = stod(strvec[1]);
    }
    if(strvec[0]=="angle"){
        angle = stod(strvec[1]);
        cosa = cos(angle);
        sina = sin(angle);
    }
}

bool Laser::is_work(double cTime, double t0, double z) const{

  return  (cTime - t0 - z >= delay  && cTime - t0 - z  <=  delay + 2 * tau);

}

bool Laser::is_work(double cTime) const{

  return  (cTime >= delay  && cTime <=  delay + 2 * tau);

}

double Laser::get_envelop(double z, double x, double cTime) const{
  double t0 = sina * (y0 - focus_d1 * sina / cosa);
  double z0 = focus_d1 * cosa;

  const double RR = 0.5 * w0 * sigma0 * sigma0;
  const double sigma = sigma0 * sqrt(1. + pow2( (z - z0) / RR) );
  const double w = 0.5*PI*(cTime - t0 - z) / tau;

  return a0*sqrt(sigma0 / sigma) * exp(-pow2(x/sigma) ) * pow2( sin(w) );

}

double3 Laser::force(double2 coord, long timestep) const{
  if (type == "VirtualDouble"){
    return force_double(coord, timestep);
  }

  return double3(0.,0.,0.);
}

double damp_func(double x, double maxsize){
    double koeff=0.1;
    double a;
   // double maxsize;

    a = (1.0-koeff)/(maxsize*maxsize);
    return a * x * x + koeff;
} 

double3 Laser::force_double(double2 coord, long timestep) const{
   double dx = 0.0001;
   double fx;
   double fy;
   double z = (coord(0) - start_d1)/vg;
   double x = coord(1);
   //double startLas;
   double cTime = timestep*Dt;
   double x1 = (x - y0 + focus_d1 * sina / cosa) * cosa - z * sina;
   double z1 = (x - y0 + focus_d1 * sina / cosa) * sina + z * cosa;
   double x2 = (x - y0 - focus_d1 * sina / cosa) * cosa + z * sina;
   double z2 = -(x - y0 - focus_d1 * sina / cosa) * sina + z * cosa;
   double t0 = sina * (y0 - focus_d1 * sina / cosa);
   double a1_z,a1_x,a1_dz,a1_dx, a2_z,a2_x,a2_dz,a2_dx;
   //std::cout<< t0 << " " << x1 << " " << z1 << " "<<x2 << " " <<z2 << "\n";
  a1_z = a1_x = a1_dz = a1_dx = a2_z = a2_x = a2_dz = a2_dx = 0.;

   if( is_work(cTime,t0,z1) ){
      a1_z = get_envelop(z1-0.5*dx,x1,cTime);
      a1_x = get_envelop(z1,x1-0.5*dx,cTime);
      a1_dz = get_envelop(z1+0.5*dx,x1,cTime);
      a1_dx = get_envelop(z1,x1+0.5*dx,cTime);

   }
    if (is_work(cTime,t0,z2) ){
      a2_z = get_envelop(z2-0.5*dx,x2,cTime) ;
      a2_x = get_envelop(z2,x2-0.5*dx,cTime) ;
      a2_dz = get_envelop(z2+0.5*dx,x2,cTime) ;
      a2_dx = get_envelop(z2,x2+0.5*dx,cTime) ;
    }

    fx = 0.25*pow2(a1_dz) - 0.25*pow2(a1_z)  +
          0.25*pow2(a2_dz) - 0.25*pow2(a2_z) + 
          0.5*a1_dz*a2_dz*cos(2*w0*sina*(x-y0)) - 0.5*a1_z*a2_z*cos(2*w0*sina*(x-y0))  ;
    fy = 0.25*pow2(a1_dx) - 0.25*pow2(a1_x) +
          0.25*pow2(a2_dx) - 0.25*pow2(a2_x) + 
          0.5*a1_dx*a2_dx*cos(2*w0*(x+0.5*dx-y0)*sina)- 0.5*a1_x*a2_x*cos(2*w0*(x-0.5*dx-y0)*sina) ;

   double damp = 1.;
   double dampSize = 10;
   if (coord(0) < DampCellsX_glob[0]*Dx + dampSize){
   		double dampCoord = coord(0)-DampCellsX_glob[0]*Dx;
   		damp = damp_func(dampCoord,dampSize);
   }
   if (coord(0) > (DampCellsX_glob[0]+PlasmaCellsX_glob)*Dx - dampSize){
   		double dampCoord = (DampCellsX_glob[0]+PlasmaCellsX_glob)*Dx - coord(0);
   		damp = damp_func(dampCoord,dampSize);
   }
   return damp*double3(-vg*fx/dx,-fy/dx,0.);

}

double3 Laser::get_field_coll(double2 x, long timestep) const{
  double f =0.;
  double z = x(0);
  double r = x(1);
  double startLas;
  double cTime = timestep*Dt;

  startLas = (z - start_d1) / vg + delay; 
  //    std::cout << z << " " <<  start_z << " "<<vg << " " <<delay  << "\n";

  if( cTime >= startLas && cTime <= startLas + 2 * tau) {
      const double RR = 0.5 * w0 * sigma0 * sigma0;
      const double sigma = sigma0 * sqrt(1. + pow2( (z - focus_d1) / RR) );
      const double w = 0.5*PI*(cTime - startLas) / tau;

      f =  a0 * w0 * (sigma0 / sigma) * exp( - pow2(r / sigma) ) * pow2(sin(w));
  }
   return double3(0.,f,0.);

}

double Laser::get_Ez(double y, double t) const{
 
    const double s2 = exp( -(y - y0) * (y - y0) / (sigma0 * sigma0) ) * cos(w0 * t);

    const double s1 = sin(0.5 * PI * t / tau);
    
    return a0 * w0 * s1 * s1 * s2;
}
double Laser::get_Ey(double x, double y, long timestep) const{
    double ctime = timestep*Dt - delay;
    double s2;
    if(focus_d1 >= 0.){
      x -= focus_d1;
      const double Rlenght = 0.5 * w0 * sigma0 * sigma0;
      const double wx = sigma0 * sqrt(1. + x * x / (Rlenght * Rlenght) );
      const double phi = atan( x / Rlenght);
      const double R = x * (1. + Rlenght * Rlenght / (x * x));
      s2 = sqrt(sigma0 / wx) * exp( - (y - y0) * (y - y0) / ( wx * wx)) 
                        * cos(w0 * ctime + phi - w0 * (y - y0) * (y - y0) * 0.5 / R);
    }
    else{
      s2 = exp( -(y - y0) * (y - y0) / (sigma0 * sigma0) ) * sin(w0 * ctime);

    }

    const double s1 = sin(0.5 * PI * ctime / tau);
    
    return a0 * w0 * s1 * s1 * s2;

}
