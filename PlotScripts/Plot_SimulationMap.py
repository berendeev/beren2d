import time
import multiprocessing
import numpy as np
import os
import matplotlib
#matplotlib.use('Qt4Agg')
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.colors as col
import matplotlib.ticker as ticker
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import gridspec
from matplotlib import rc
import pprint
import collections


#чтение параметров расчёта
from ReadDataLib import *
from LibPlot import *

#корневой каталог для расчёта (где файл параметров)
WorkDir='../'
Rewrite = True

FieldsAmp=0.15 #амплитуда палитры для полей

#словарь с системными параметрами
SystemParameters=ReadParameters()
### Флаги диагностики полей [Ex,Ey,Ez,Bx,By,Bz,Jx,Jy,Jz]
SystemParameters['2D_Diag_Fields']=['1','1','1','1','1','1','1','1','1'] 
LenFieldsDiag = len(SystemParameters['2D_Diag_Fields'])

FileNameSignNum=3 #len(str(int(int(SystemParameters['Max_Time'][0])/int(SystemParameters['Diagn_Time'][0]))))
print(FileNameSignNum)

def SubPlot(x,y,fig):
	return fig.add_subplot(gs[y,x])


tdelay = int(SystemParameters['TimeDelay']) * float( SystemParameters['Dt'])

Fieldfiles = os.listdir(WorkDir+'Fields/Diag2D/') #получили список файлов в каталоге с полями (они есть всегда)
Graphfiles = os.listdir(WorkDir+'Anime/') #получили список файлов в каталоге с графиками
Fieldfiles.sort()

i = len(Fieldfiles)-1 #-rank #StartNum+  rank
print(SystemParameters)

try:
	for sort in SystemParameters['Particles'].keys():
		os.mkdir('../Anime/'+sort)
except OSError:
      print("Подкаталоги для частиц существуют")


try:
      os.mkdir('../Anime/Fields')
except OSError:
      print("Подкаталог для полей существуют")


while i >= 0:
	
	TimeStep = Fieldfiles[i][-FileNameSignNum:]
	#имя файла результирующего
	GraphName='Res'+TimeStep+'.png'
	#проверка на наличие уже построенного графика
	if(Rewrite or GraphName not in Graphfiles):
		print('TimeStep=',TimeStep)

		fig = plt.figure(figsize=(int(SystemParameters['NumOfPartSpecies'])*4+4*((LenFieldsDiag+2)//3), 12))

		#на основе того, сколько было сортов частиц определить сетку для графиков
		gs =gridspec.GridSpec(4,int(SystemParameters['NumOfPartSpecies'])+(LenFieldsDiag+2)//3,height_ratios=[0.1,1,1,1]) #первый аргумент - вертикальное количество, второй - горизонтальное (по горизонтале число сортов частиц + 2 столбца для полей

			
		#считываем плотности
		DensData = collections.OrderedDict()
		for sort in SystemParameters['Particles'].keys():
			DensData.update(ReadDensFile(WorkDir,SystemParameters,sort,TimeStep))
#
		PhaseData = collections.OrderedDict()
		for sort in SystemParameters['Particles'].keys():
			PhaseData.update(ReadPhaseFile(WorkDir,SystemParameters,sort,TimeStep))

		FieldData = ReadFieldsFile(WorkDir,SystemParameters,TimeStep)

		gs_x=0
		for sort in SystemParameters['Particles'].keys():
			Plot2Ddens(DensData,sort,0,2*float(SystemParameters['Particles'][sort]['Dens']),sort,SystemParameters,SubPlot(gs_x,1,fig),fig)
			Plot2DPhase(PhaseData,sort,0,2*float(SystemParameters['Particles'][sort]['Dens']),sort,SystemParameters,SubPlot(gs_x,3,fig),fig)
			gs_x+=1
		
		gs_x=0
		gs_y=2
		for sort in SystemParameters['Particles'].keys():
			Plot1Ddens(DensData,sort,'long',0,4*float(SystemParameters['Particles'][sort]['Dens']),'',SystemParameters,fig.add_subplot(gs[gs_y,gs_x], label="1"),fig)
			gs_x+=1

		gs_x=-2 #поля начинаются со второго справа столбика
		gs_y=1 #первая строка
		
		Plot2Dfields(FieldData,"Ex",-FieldsAmp,FieldsAmp,"Ex",SystemParameters,SubPlot(-2,1,fig),fig)
		Plot2Dfields(FieldData,"Ey",-FieldsAmp,FieldsAmp,"Ey",SystemParameters,SubPlot(-2,2,fig),fig)
		Plot2Dfields(FieldData,"Ez",-FieldsAmp,FieldsAmp,"Ez",SystemParameters,SubPlot(-2,3,fig),fig)
		Plot1Dfields(FieldData,"Ex","x",-FieldsAmp,FieldsAmp,"Ex",SystemParameters,SubPlot(-1,1,fig),fig)

		#Plot2Dfields(FieldData,"Bx",float(SystemParameters['UniformB'][0])-FieldsAmp,float(SystemParameters['UniformB'][0])+FieldsAmp,"Bx",SystemParameters,SubPlot(-1,1,fig),fig)
		Plot2Dfields(FieldData,"By",-FieldsAmp,FieldsAmp,"By",SystemParameters,SubPlot(-1,2,fig),fig)
		Plot2Dfields(FieldData,"Bz",-FieldsAmp,FieldsAmp,"Bz",SystemParameters,SubPlot(-1,3,fig),fig)
		Plot1Dfields(FieldData,"Jx","y",-FieldsAmp,FieldsAmp,"Jx",SystemParameters,SubPlot(-3,1,fig),fig)
		#Plot2Dfields(FieldData,"Jx",-FieldsAmp,FieldsAmp,"Jx",SystemParameters,SubPlot(-3,1,fig),fig)
		Plot1Dfields(FieldData,"Jy","y",-FieldsAmp,FieldsAmp,"Jy",SystemParameters,SubPlot(-3,2,fig),fig)
		#Plot2Dfields(FieldData,"Jy",-FieldsAmp,FieldsAmp,"Jy",SystemParameters,SubPlot(-3,2,fig),fig)
		#Plot2Dfields(FieldData,"Jz",-FieldsAmp,FieldsAmp,"Jz",SystemParameters,SubPlot(-3,3,fig),fig)

		MaxTime=int(TimeStep)*tdelay
		MaxTimeDt=round(MaxTime,1)

		plt.figtext(0.5, 0.96,  r'$t\cdot\omega_p = '+str(MaxTimeDt)+'$',bbox=dict(boxstyle='round', facecolor='wheat', alpha=0.5),
						color='black',fontsize=18,ha='center')

		plt.tight_layout()
		plt.savefig('../Anime/'+GraphName, format='png', dpi=150)
		plt.close(fig)

	i = i-1
