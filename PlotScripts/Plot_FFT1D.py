import os
import gc
import numpy as np
import matplotlib
#matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib import rc
import matplotlib.ticker as ticker
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import ScalarFormatter

from numpy.fft import rfft, rfftfreq
from math import sin, pi,sqrt

from numpy import array, arange,amax, argmax,sign, abs as np_abs

fig = plt.figure(figsize=(10, 5))

gs = gridspec.GridSpec(1,2)#первый аргумент - вертикальное количество, второй - горизонтальное (по горизонтале число сортов частиц + 2 столбца для полей

fname = "Zond7"
FontSize = 25
dt = 0.1 ### Timestep

ttime,Field1d = np.genfromtxt(r'../Fields/Zond007.dat', unpack=True, usecols=(0,1),skip_header=1) #np.load('1dSignal.npy')


def SubPlot(x,y,fig):
	return fig.add_subplot(gs[y,x])

from scipy.interpolate import splrep, sproot, splev
def fwhm(x, y, k=10):
    """
    Determine full-with-half-maximum of a peaked set of points, x and y.

    Assumes that there is only one peak present in the datasset.  The function
    uses a spline interpolation of order k.
    """

    class MultiplePeaks(Exception): pass
    class NoPeaksFound(Exception): pass

    half_max = amax(y)/2.0
    s = splrep(x, y - half_max)
    #print(s)

    roots = sproot(s)
   # if len(roots) > 2:
    #    raise MultiplePeaks("The dataset appears to have multiple peaks, and "
              #  "thus the FWHM can't be determined.")
   # elif len(roots) < 2:
    #    raise NoPeaksFound("No proper peaks were found in the data set; likely "
              #  "the dataset is flat (e.g. all zeros).")
    #else:
    return abs(roots[1] - roots[0])

def lin_interp(x, y, i, half):
    return x[i] + (x[i+1] - x[i]) * ((half - y[i]) / (y[i+1] - y[i]))

def half_max_x(x, y):
    half = max(y)/2.0
    signs = np.sign(np.add(y, -half))
    zero_crossings = (signs[0:-2] != signs[1:-1])
    zero_crossings_i = np.where(zero_crossings)[0]
    return [lin_interp(x, y, zero_crossings_i[0], half),
            lin_interp(x, y, zero_crossings_i[1], half)]
    
#Фурье
def FFT(Signal,step):
	N=len(Signal)
	FD = 1./step # частота дискретизации, отсчётов в секунду
	spectrum = rfft(Signal)
	fftx=rfftfreq(N, 1./FD)*2*pi
	Rad_freq=fftx[argmax( np_abs(spectrum))]
	
	hmx = half_max_x(fftx,np_abs(spectrum)/N)

# print the answer
	WidthSp= hmx[1] - hmx[0] #fwhm(fftx,np_abs(spectrum)/N,10)
	#WidthSp= fwhm(fftx,np_abs(spectrum)/N,10)

	print("hwm=",hmx[0],hmx[1])
	print("Max_freq=",Rad_freq)
	print("width=",WidthSp)
	print("width=",WidthSp/Rad_freq*100,"%")
	Spectr={}
	Spectr['WidthSp']=WidthSp
	Spectr['spectrum']=spectrum
	Spectr['fftx']=fftx
	Spectr['Rad_freq']=Rad_freq
	Spectr['N']=N
	Spectr['MaxSp']=amax( np_abs(spectrum))/N
	return Spectr
	

axZond=SubPlot(0,0,fig)

axZond.plot(ttime,Field1d,color='black',lw=2.5)
axZond.xaxis.set_major_locator(ticker.MaxNLocator(nbins=6))


Spectr = FFT(Field1d,dt)#спектр по времени (w)

# ax1.title.set_text('$\Omega/\omega_p='+SysPar['UniformB'][0])
axFFT = SubPlot(1,0,fig)
axFFT.plot(Spectr['fftx'], np_abs(Spectr['spectrum'])/Spectr['N'],color='green',lw=2.5)

axFFT.arrow(Spectr['Rad_freq']-1.15, Spectr['MaxSp']*0.5, 0.5, 0,width=0.00005, head_width =0.0002, head_length=0.5,fc='k', ec='k')
axFFT.arrow(Spectr['Rad_freq']+1.15, Spectr['MaxSp']*0.5, -0.5, 0, width=0.00005,head_width = 0.0002, head_length=0.5, fc='k', ec='k')

axFFT.text(3.0, Spectr['MaxSp']*0.6,  str(r'$\frac{\Delta\omega}{\omega}{=}'+'{:.2f}'.format(Spectr['WidthSp']/Spectr['Rad_freq']*100)+'\%$'),color='black',fontsize=15,ha='center',va='bottom')
axFFT.text(Spectr['Rad_freq'], Spectr['MaxSp']*1.05,  '{:.1f}'.format(Spectr['Rad_freq'])+'$\omega_p$',color='red',fontsize=14,ha='center',va='bottom')
axFFT.set_xlim(0,4)
axFFT.xaxis.set_major_locator(ticker.MaxNLocator(nbins=6))
axFFT.legend()

axZond.set_xlabel('$time$, $1/\omega_p$',fontsize = FontSize)
axFFT.set_xlabel('$\omega$',fontsize = FontSize)
axZond.tick_params(axis='x', length = 4,width=2,labelsize = FontSize-5,top=True,direction = 'in',pad =10)
axZond.tick_params(axis='y', length = 4,width=2,labelsize = FontSize-5,top=True,direction = 'in',pad =10)
axFFT.tick_params(axis='x', length = 4,width=2,labelsize = FontSize-5,top=True,direction = 'in',pad =10)
axFFT.tick_params(axis='y', length = 4,width=2,labelsize = FontSize-5,top=True,direction = 'in',pad =10)


formatter = ScalarFormatter()
formatter.set_powerlimits((0, 0))
axFFT.yaxis.set_major_locator(ticker.MaxNLocator(nbins=6))
axFFT.yaxis.set_major_formatter(formatter)

plt.tight_layout()

try:
	os.mkdir('../Anime/Zonds')
except OSError:
      print("Подкаталоги для зондов существуют")

#print("dw/w = ",Spectr['WidthSp']/Spectr['Rad_freq']*100,'%',"\n maxSp = ", Spectr['MaxSp'],"\nFreq = ",Spectr['Rad_freq'])

# Save figure handle to disk

plt.savefig('../Anime/Zonds/'+fname+'.png',dpi=150,format='png')

#plt.show()
