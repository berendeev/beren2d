import numpy as np
import matplotlib.pyplot as plt
energy = {}
energy["time"],energy["radY1"],energy["radY2"] = np.genfromtxt(r'../Energies.dat', unpack=True, usecols=(0,12,13),skip_header=1)

fig, ax = plt.subplots(figsize=(8, 6))

ax.plot(energy["time"],energy["radY1"]+energy["radY2"], color="k",lw=2.0) #,label="$\sigma=40$")
ax.set_xlabel('$time, 1/\omega_p$',fontsize=22)
ax.yaxis.grid()

#ax.set_xlim(250,500)
ax.tick_params(axis='x',labelsize = 25)
ax.set_title("$Radiation$",fontsize=22)
plt.legend(loc = 'best',fontsize=11)	
plt.tight_layout(pad =1.08)

try:
	os.mkdir('../Anime/Energy')
except OSError:
      print("Подкаталоги для энергий существуют")

plt.savefig('../Anime/Energy/Energy.png', format='png', dpi=150)

