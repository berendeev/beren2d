# Python script for setting parameters
import pprint
import sys
sys.path.insert(0, "./Scripts")
from setInitParams import *
from math import asin, sqrt,sin

DirName = "Res_Beren2D_1.5BigAreaLongTime"


DampType = enum("NONE","DAMP","PML")

DAMP_FIELDS = DampType.DAMP

BoundType = enum("NONE","PERIODIC","OPEN","NEIGHBOUR")

BoundTypeX_glob = [BoundType.OPEN, BoundType.OPEN]
BoundTypeY_glob = BoundType.OPEN


Queue = "48" # type of queue
Cluster = "nks1p" # cluster name (spb, nsu, sscc)

####
RECOVERY = 0

DEBUG = 1

SHAPE = 2  # 1 - PIC, 2 - Parabolic e.t.c

IONIZATION = 0 # Fields ionization

PARTICLE_MASS = False #True ### Particle has individual mass
PARTICLE_MPW = False ### Particle has individual macro particle weight

UPD_PARTICLES = 1

UPD_FIELDS = 1 # Update Fields (for Debug)


#####

NumProcs = 48*2 # number of processors
NumAreas = 48 # Number of decomposition region


Dx = 0.05 # step on X
Dy = 0.05 # step on Y

scale = 2

laserRightPathCells = scale*200

PlasmaCellsX_glob = scale*5300 + laserRightPathCells # Number of cells for Plasma on Z

PlasmaCellsY_glob = scale*1000 # Number of cells for Plasma on R 

NumCellsY_glob = scale*1600 # NumbeY of all cells in computation domain on R

damp =70*scale
DampCellsX_glob = [damp,damp] # Number of Damping layer cells on Z
DampCellsXP_glob = [scale*10,scale*10] # Number of Damping layer cells on Z near Plasma 
DampCellsY_glob = damp # Number of Damping layer cells on Y

NumCellsX_glob = PlasmaCellsX_glob + DampCellsX_glob[0]+DampCellsX_glob[1] # Number of all cells in computation domain on Z


sigma0 = 30.

NumPartPerLine = 8 # Number of particles per line segment cell 
NumPartPerCell = NumPartPerLine * NumPartPerLine # Number of particles per cell

MaxTime = 1800 # in 1/w_p
RecTime = 1601 #


DiagDelay2D = 4 # in 1 / w_p
DiagDelay1D = 1 # in 1 / w_p
DiagDelayEnergy1D = 1 

BUniform = [0., 0.0, 0.0] # in w_c / w_p

###### PHYSIC CONST
PI = 3.141592653589793
me = 9.10938356e-28 # electron mass
ee = 4.80320427e-10 # electron charge
n0 = 5.e15 # particles / cm^3
n0 = 2.5e18 # particles / cm^3
w_p = (4*PI*n0*ee*ee/me)**0.5
cc = 2.99792458e10 # speed on light cm/sec 
MC2 = 512.
########



#######################################


Dt =  0.5*min(Dx,Dy)  # time step

MaxTimeStep = int(round(MaxTime/Dt+1))
RecTimeStep = int(round(RecTime/Dt))
StartTimeStep = int(round(RECOVERY/Dt))
TimeStepDelayDiag2D = int(round(DiagDelay2D/Dt))
TimeStepDelayDiag1D = int(round(DiagDelay1D/Dt))


#### ZONDS ##############
numZonds = 15
### First number - number of zonds
zondX = list(PlasmaCellsX_glob / (numZonds + 1) * Dx * (x+1) + Dx* DampCellsX_glob[0] for x in range(numZonds)  )
zondY = [ Dy * (NumCellsY_glob - DampCellsY_glob - 30*scale) ] * numZonds

if len(zondX) != len(zondY):
  print("Diff numbers of zonds coord\n")
  exit()
zondCoords = list(zip(zondX,zondY) )

### First number - number of zonds
centerY = 0.5*Dy*NumCellsY_glob
zondCoordsLineY = [centerY, Dy * (DampCellsY_glob + 30*scale), Dy * (NumCellsY_glob - DampCellsY_glob - 30*scale)]
zondCoordsLineX = [0.5*Dx*NumCellsX_glob] #[Dx * (DampCellsX_glob[0] + 10)]

########################################
#### Coords for radiation diagnostic
sliceRadiationLineY = [Dy * (NumCellsY_glob - DampCellsY_glob - 30*scale),Dy * ( DampCellsY_glob + 30*scale) ]
sliceRadiationLineX = [Dx * (DampCellsX_glob[0] + 10*scale), Dx * (NumCellsX_glob - DampCellsX_glob[1] - 10*scale)]



PxMax = 800 // NumAreas
PpMax = 800

MaxSizeOfParts=NumPartPerCell*PlasmaCellsX_glob*PlasmaCellsY_glob/NumProcs+1


NumOfPartSpecies = 0

PartParams = {} # 
 # 
isVelDiag = 0


PName="Neutrals"

Exist = False
PartDict = {}
PartDict["Charge"] = 0.0
PartDict["Density"] = 0.5
PartDict["Velocity"] = 0.0
PartDict["Mass"] = 4.*1836.0
Pot_I = 0.02459 # Kev
PartDict["Pot_I"] = Pot_I
PartDict["Pot_k"] = 1.
PartDict["Temperature"] = 0;# (0.014/512.)**0.5
PartDict["Px_max"] = 1.0 # 
PartDict["Px_min"] = -1.0 #
PartDict["Width"] = PlasmaCellsY_glob * Dy
PartDict["Shift"] = 0.0
PartDict["SmoothMass"] = 1
InitDist = "StrictUniform"
dn0 = 0.0
k0=3.55
PartDict["DistParams"] = [str(InitDist), dn0, k0]



if Exist :
    NumOfPartSpecies+=1
    setParams(PartParams, PName, PartDict)


PName="Electrons"

Exist = True
PartDict = {}
PartDict["Charge"] = -1.0
PartDict["Density"] = 1.0 #0.5
PartDict["Velocity"] = 0.0
PartDict["Mass"] = 1.0
PartDict["Temperature"] = (0.014/512.)**0.5
PartDict["Px_max"] = 1.e-1 # 
PartDict["Px_min"] = -1.e-1 #
PartDict["Width"] = PlasmaCellsY_glob * Dy
PartDict["Shift"] = 0.0
PartDict["SmoothMass"] = 0.0
PartDict["BoundResumption"] = 1
InitDist = "None"
InitDist = "StrictUniform"
dn0 = 0.0
k0=3.55
PartDict["DistParams"] = [str(InitDist), dn0, k0]


if Exist:
    NumOfPartSpecies+=1
    setParams(PartParams, PName, PartDict)

PName="Ions"

Exist = False#True
PartDict = {}
PartDict["Charge"] = 1.0
PartDict["Density"] = 1.0
PartDict["Velocity"] = 0.0
PartDict["Mass"] = 1836.0 * 4
PartDict["Temperature"] = 0.0
PartDict["Px_max"] = 1.0 # 
PartDict["Px_min"] = -1.0 #
PartDict["Width"] = PlasmaCellsY_glob * Dy
PartDict["Shift"] = 0.0
PartDict["SmoothMass"] = 0
PartDict["SmoothMassMax"] = 30
PartDict["SmoothMassSize"] = 15
PartDict["BoundResumption"] = 1

InitDist = "StrictUniform"
InitDist = "None"
dn0 = 0.0
k0=3.55
PartDict["DistParams"] = [str(InitDist), dn0, k0]
Pot_I = 0.05442 # Kev
PartDict["Pot_I"] = Pot_I
PartDict["Pot_k"] = 2.

#SmoothMassMax = 800.
#SmoothMassSize = 0.15*PlasmaCellsZ_glob * Dz

if Exist :
    NumOfPartSpecies+=1
    setParams(PartParams, PName, PartDict)


PName="Ions2"

Exist = False
PartDict = {}
PartDict["Charge"] = 2.0
PartDict["Density"] = 0.5
PartDict["Velocity"] = 0.0
PartDict["Mass"] = 4.*1836.0
PartDict["Temperature"] = 0.0
PartDict["SmoothMass"] = 0.0
PartDict["Px_max"] = 1.0 # 
PartDict["Px_min"] = -1.0 #
PartDict["Width"] = PlasmaCellsY_glob*Dy
PartDict["Shift"] = 0.0
PartDict["BoundResumption"] = 1

InitDist = "None" #"StrictUniform"
dn0 = 0.0
k0=3.55
PartDict["DistParams"] = [str(InitDist), dn0, k0]

SmoothMassMax = 40.
SmoothMassSize = 10. #0.15*PlasmaCellsZ_glob*Dz

if Exist :
    NumOfPartSpecies+=1
    setParams(PartParams, PName, PartDict)



###### GLOBAL BEAMS PARAMETERS #####################
InjType = 0 # 0 - random inject, 1 - periodic strit inject
InjectSmoothTime = 0.001

Vb = 0.9#79 #0.995

SourceType = enum("NONE","FOCUSED_GAUSS","UNIFORM")
#### FOCUSED BEAMS 

Lmax = 10. / (cc / w_p) # cm / c/w_p
Larea = 0.5*PlasmaCellsX_glob*Dx
Rmax = 2.5 / (cc / w_p) # cm / c/w_p
Rfocus = 0.05  / (cc / w_p) # cm / c/w_p
Xfocus = 0.5 * Dx * PlasmaCellsX_glob



PName="BeamLeft"

Exist = False
PartDict = {}
PartDict["Charge"] = -1.0
PartDict["Density"] = 0.01
PartDict["Velocity"] = 0.3 #Vb
PartDict["Mass"] = 1.0
PartDict["Temperature"] = (0./512.)**0.5
PartDict["Px_max"] = 1.0
PartDict["Px_min"] = 0.0
PartDict["Width"] = Dy * PlasmaCellsY_glob
PartDict["Focus"] = 0.5 * Dx * PlasmaCellsX_glob
PartDict["SourceType"] = SourceType.UNIFORM#FOCUSED_GAUSS
PartDict["Shift"] = 0.0
InitDist = "None"
PartDict["DistParams"] = [str(InitDist)]


if Exist :
    NumOfPartSpecies+=1
    setParams(PartParams, PName, PartDict)

PName="BeamRight"

Exist = False
PartDict = {}
PartDict["Charge"] = -1.0
PartDict["Density"] = 0.002
PartDict["Velocity"] = -Vb
PartDict["Mass"] = 1.0
PartDict["Temperature"] = (4./512.)**0.5
PartDict["Px_max"] = 0.0
PartDict["Px_min"] = -8.0
PartDict["Width"] = Dy * PlasmaCellsY_glob
PartDict["Focus"] = 0.5 * Dx * PlasmaCellsX_glob
PartDict["SourceType"] = SourceType.UNIFORM #FOCUSED_GAUSS
PartDict["Shift"] = 0.0
InitDist = "None"
PartDict["DistParams"] = [str(InitDist)]

if Exist :
    NumOfPartSpecies+=1
    setParams(PartParams, PName, PartDict)
    

### GLOBAL LASERS PARAMETERS ##############################
Las_tau = 3.48
Las_w0 = 25.44
Las_vg = (1. - 1. / (Las_w0**2) )**0.5

LasParams = {} # 

angle = asin(0.5*sqrt(3.)/Las_w0)

a0 = 0.3

y_shift = PI/sqrt(3.) # (4.*Las_w0*sin(angle))  #0.5*PI/sqrt(3.)

DelayLeft =0.
DelayRight = 0.
LasName="LaserLeft"
Exist = True
LasDict = {} #
LasDict["delay"] = 0#DelayLeft*Las_tau
LasDict["tau"] = Las_tau
LasDict["type"] = "VirtualDouble"
LasDict["vg"] = 1.#Las_vg
LasDict["a0"] = a0
LasDict["angle"] = angle #1./50.
LasDict["sigma0"] = sigma0
LasDict["w0"] = Las_w0
LasDict["focus_x"] =  Dx * PlasmaCellsX_glob - Dx*laserRightPathCells
LasDict["y0"] =  0.5 * Dy * NumCellsY_glob
LasDict["start_x"] = Dx*DampCellsX_glob[0] 

if Exist:
    setParams(LasParams, LasName, LasDict)

LasName="LaserRight"
Exist = True
LasDict = {} #
LasDict["delay"] = 0#DelayRight*Las_tau
LasDict["tau"] = Las_tau
LasDict["vg"] = -1.#Las_vg
LasDict["a0"] = a0
LasDict["sigma0"] =  sigma0
LasDict["angle"] = angle #1./50.

LasDict["type"] = "VirtualDouble"
LasDict["w0"] = Las_w0
LasDict["y0"] =  0.5 * Dy * NumCellsY_glob + y_shift
LasDict["focus_x"] =  Dx*PlasmaCellsX_glob - Dx*laserRightPathCells #0.5 * Dx * PlasmaCellsX_glob
LasDict["start_x"] = 2*Dx*PlasmaCellsX_glob + Dx*DampCellsX_glob[0] - 2*Dx*laserRightPathCells 

if Exist:
    setParams(LasParams, LasName, LasDict)

HalfPointCoordX = Dx*PlasmaCellsX_glob + Dx*DampCellsX_glob[0] - Dx*laserRightPathCells 

#####//////////////////////////////

WorkDir = DirName+"_addRightCells_"+str(laserRightPathCells)+"_a0_"+str(a0)+"_ny_"+str(PlasmaCellsY_glob)+"_np_"+str(NumPartPerCell )+"_Dx_"+str(Dx)+"_angle_"+str(angle)+"_shift_"+str(y_shift)+"_sigma0_"+str(sigma0)






if SHAPE < 3:
    SHAPE_SIZE = 2
    CELLS_SHIFT = 1
else:
    SHAPE_SIZE = 3
    CELLS_SHIFT = 2    

ADD_NODES = 2 * CELLS_SHIFT + 1


if PlasmaCellsX_glob % NumAreas != 0:
	print("***********************************************")
	print("WARNING!!! Domain decomposition is not correct!!")
	print("***********************************************")

###////////////////////////////////
###////////////////////////////////
DiagParams = {}
DiagDict = {}
DiagDict["zondCoords"] = zondCoords
DiagDict["zondCoordsLineX"] = zondCoordsLineX
DiagDict["zondCoordsLineY"] = zondCoordsLineY
DiagDict["sliceRadiationLineX"] = sliceRadiationLineX
DiagDict["sliceRadiationLineY"] = sliceRadiationLineY


setParams(DiagParams, "Diagnostics", DiagDict)

DefineParams = []
SysParams = []

setConst(SysParams,'const long','NumProcs',[NumProcs],None)
setConst(SysParams,'const long','NumAreas',[NumAreas],None)

setConst(SysParams,'const double','Dx',[Dx],None)
setConst(SysParams,'const double','Dy',[Dy],None)
setConst(SysParams,'const double','Dt',[Dt],None)

setConst(SysParams,'const long','NumCellsX_glob',[NumCellsX_glob],None)
setConst(SysParams,'const long','NumCellsY_glob',[NumCellsY_glob],None)
setConst(SysParams,'const long','NumCellsXmax_glob',[NumCellsX_glob + 2 * SHAPE_SIZE - 1],None)
setConst(SysParams,'const long','NumCellsYmax_glob',[NumCellsY_glob + 2 * SHAPE_SIZE - 1],None)
setConst(SysParams,'const long','DampCellsX_glob',DampCellsX_glob,None)
setConst(SysParams,'const long','DampCellsXP_glob',DampCellsXP_glob,None)
setConst(SysParams,'const long','DampCellsY_glob',[DampCellsY_glob],None)
setConst(SysParams,'const long','PlasmaCellsY_glob',[PlasmaCellsY_glob],None)
setConst(SysParams,'const long','PlasmaCellsX_glob',[PlasmaCellsX_glob],None)

setConst(SysParams,'const long','NumOfPartSpecies',[NumOfPartSpecies],None)
setConst(SysParams,'const long','NumPartPerLine ',[NumPartPerLine ],None)
setConst(SysParams,'const long','NumPartPerCell',[NumPartPerCell],None)

setConst(SysParams,'const long','MaxTimeStep',[MaxTimeStep],None)
setConst(SysParams,'const long','RecTimeStep',[RecTimeStep],None)
setConst(SysParams,'const long','StartTimeStep',[StartTimeStep],None)
setConst(SysParams,'const long','TimeStepDelayDiag1D',[TimeStepDelayDiag1D],None)
setConst(SysParams,'const long','TimeStepDelayDiag2D',[TimeStepDelayDiag2D],None)
#setConst(SysParams,'const long','EnergyOutputDelay1D',[EnergyOutputDelay1D],None)

setConst(SysParams,'const double','InjectSmoothTime',[InjectSmoothTime],None)

setConst(SysParams,'const double','BUniform',BUniform,None)
setConst(SysParams,'const long','BoundTypeY_glob',[BoundTypeY_glob],None)
setConst(SysParams,'const long','BoundTypeX_glob',BoundTypeX_glob,None)

setConst(SysParams,'const double','MC2',[MC2],None)
setConst(SysParams,'const double','n0',[n0],None)
#setConst(SysParams,'const double','zondY',zondY,None)
#setConst(SysParams,'const double','zondX',zondX,None)
#setConst(SysParams,'const double','zondLineX',zondLineX,None)
#setConst(SysParams,'const double','zondLineY',zondLineY,None)

#setConst(SysParams,'const double','zondRadY',zondRadY,None)
#setConst(SysParams,'const double','zondRadX',zondRadX,None)

setConst(SysParams,'const int','isVelDiag',[isVelDiag],None)
setConst(SysParams,'const long','PxMax',[PxMax],None)
setConst(SysParams,'const long','PpMax',[PpMax],None)

setConst(SysParams,'const long','MaxSizeOfParts',[MaxSizeOfParts],None)

setConst(SysParams,'const double','PI',[PI],None)

setConst(SysParams,'const double','HalfPointCoordX',[HalfPointCoordX],None)
setConst(SysParams,'const double','Rmax',[Rmax],None)
setConst(SysParams,'const double','Rfocus',[Rfocus],None)
setConst(SysParams,'const double','Lmax',[Lmax],None)
setConst(SysParams,'const double','Larea',[Larea],None)
setConst(SysParams,'const double','SmoothMassMax',[SmoothMassMax],None)
setConst(SysParams,'const double','SmoothMassSize',[SmoothMassSize],None)

if DEBUG == 0:
    setConst(DefineParams,'#define','NDEBUG',[' '],None)
setConst(DefineParams,'#define','DEBUG',[DEBUG],None)
if PARTICLE_MASS:
    setConst(DefineParams,'#define','PARTICLE_MASS',[1],None)
if PARTICLE_MPW:
    setConst(DefineParams,'#define','PARTICLE_MPW',[1],None)
setConst(DefineParams,'#define','RECOVERY',[RECOVERY],None)
setConst(DefineParams,'#define','SHAPE',[SHAPE],None)
setConst(DefineParams,'#define','SHAPE_SIZE',[SHAPE_SIZE],None)
setConst(DefineParams,'#define','CELLS_SHIFT',[CELLS_SHIFT],None)
setConst(DefineParams,'#define','ADD_NODES',[ADD_NODES],None)
setConst(DefineParams,'#define','IONIZATION',[IONIZATION],None)

setConst(DefineParams,'#define','DEBUG',[DEBUG],None)
setConst(DefineParams,'#define','UPD_FIELDS',[UPD_FIELDS],None)
setConst(DefineParams,'#define','UPD_PARTICLES',[UPD_PARTICLES],None)
setConst(DefineParams,'#define','DAMP_FIELDS',[DAMP_FIELDS],None)

setConst(DefineParams,'#define','PML',[DampType.PML],None)
setConst(DefineParams,'#define','DAMP',[DampType.DAMP],None)
setConst(DefineParams,'#define','PERIODIC',[BoundType.PERIODIC],None)
setConst(DefineParams,'#define','OPEN',[BoundType.OPEN],None)
setConst(DefineParams,'#define','NEIGHBOUR',[BoundType.NEIGHBOUR],None)
setConst(DefineParams,'#define','SOURCE_FOCUSED_GAUSS',[SourceType.FOCUSED_GAUSS],None)
setConst(DefineParams,'#define','SOURCE_UNIFORM',[SourceType.UNIFORM],None)
setConst(DefineParams,'#define','SOURCE_NONE',[SourceType.NONE],None)



writeParams("Particles","PartParams.cfg",PartParams)
writeParams("Lasers","LasParams.cfg",LasParams)
writeParams("Diagnostics","Diagnostics.cfg",DiagParams)

writeConst('const.h', 'SysParams.cfg', SysParams)
writeDefine('defines.h',DefineParams)



f = open('phys.par', 'w')
f.write("w_p = " + str(w_p))
f.write("1/w_p = " + str(1./w_p))
f.close()

f = open('workdir.tmp', 'w')
f.write(WorkDir)
f.close()
f = open('queue.tmp', 'w')
f.write(Queue)
f.close()
f = open('cluster.tmp', 'w')
f.write(Cluster)
f.close()
f = open('proc.tmp', 'w')
f.write(str(NumProcs))
f.close()
